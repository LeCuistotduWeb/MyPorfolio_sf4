/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/resources/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/js/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/js/app.js":
/*!**************************!*\
  !*** ./assets/js/app.js ***!
  \**************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

// /*
//  * Welcome to your app's main JavaScript file!
//  *
//  * We recommend including the built version of this JavaScript file
//  * (and its CSS file) in your base layout (base.html.twig).
//  */
//
// // any CSS you require will output into a single css file (app.css in this case)
// require('../css/app.css');
//
// // Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// // var $ = require('jquery');
//
// console.log('Hello Webpack Encore! Edit me in assets/js/app_admin.js');
<<<<<<< HEAD

(function ($) {
    $(function () {

        // $('.button-collapse').sideNav();
        // $('.scrollspy').scrollSpy();
        // $(".dropdown-trigger").dropdown();

        /*** Animate word ***/

=======
(function ($) {
    $(function () {
        /*** Animate word ***/
>>>>>>> dev
        //set animation timing
        var animationDelay = 2500,

        //loading bar effect
        barAnimationDelay = 3800,
            barWaiting = barAnimationDelay - 3000,
            //3000 is the duration of the transition on the loading bar - set in the scss/css file
        //letters effect
        lettersDelay = 50,

        //type effect
        typeLettersDelay = 150,
            selectionDuration = 500,
            typeAnimationDelay = selectionDuration + 800,

        //clip effect
        revealDuration = 600,
            revealAnimationDelay = 1500;

        initHeadline();

        function initHeadline() {
            singleLetters($('.cd-headline.letters').find('b'));
            animateHeadline($('.cd-headline'));
        }

        function singleLetters($words) {
            $words.each(function () {
                var word = $(this),
                    letters = word.text().split(''),
                    selected = word.hasClass('is-visible');
                for (i in letters) {
                    if (word.parents('.rotate-2').length > 0) letters[i] = '<em>' + letters[i] + '</em>';
                    letters[i] = selected ? '<i class="in">' + letters[i] + '</i>' : '<i>' + letters[i] + '</i>';
                }
                var newLetters = letters.join('');
                word.html(newLetters).css('opacity', 1);
            });
        }

        function animateHeadline($headlines) {
            var duration = animationDelay;
            $headlines.each(function () {
                var headline = $(this);

                if (headline.hasClass('loading-bar')) {
                    duration = barAnimationDelay;
                    setTimeout(function () {
                        headline.find('.cd-words-wrapper').addClass('is-loading');
                    }, barWaiting);
                } else if (headline.hasClass('clip')) {
                    var spanWrapper = headline.find('.cd-words-wrapper'),
                        newWidth = spanWrapper.width() + 10;
                    spanWrapper.css('width', newWidth);
                } else if (!headline.hasClass('type')) {
                    //assign to .cd-words-wrapper the width of its longest word
                    var words = headline.find('.cd-words-wrapper b'),
                        width = 0;
                    words.each(function () {
                        var wordWidth = $(this).width();
                        if (wordWidth > width) width = wordWidth;
                    });
                    headline.find('.cd-words-wrapper').css('width', width);
                };

                //trigger animation
                setTimeout(function () {
                    hideWord(headline.find('.is-visible').eq(0));
                }, duration);
            });
        }

        function hideWord($word) {
            var nextWord = takeNext($word);

            if ($word.parents('.cd-headline').hasClass('type')) {
                var parentSpan = $word.parent('.cd-words-wrapper');
                parentSpan.addClass('selected').removeClass('waiting');
                setTimeout(function () {
                    parentSpan.removeClass('selected');
                    $word.removeClass('is-visible').addClass('is-hidden').children('i').removeClass('in').addClass('out');
                }, selectionDuration);
                setTimeout(function () {
                    showWord(nextWord, typeLettersDelay);
                }, typeAnimationDelay);
            } else if ($word.parents('.cd-headline').hasClass('letters')) {
                var bool = $word.children('i').length >= nextWord.children('i').length ? true : false;
                hideLetter($word.find('i').eq(0), $word, bool, lettersDelay);
                showLetter(nextWord.find('i').eq(0), nextWord, bool, lettersDelay);
            } else if ($word.parents('.cd-headline').hasClass('clip')) {
                $word.parents('.cd-words-wrapper').animate({ width: '2px' }, revealDuration, function () {
                    switchWord($word, nextWord);
                    showWord(nextWord);
                });
            } else if ($word.parents('.cd-headline').hasClass('loading-bar')) {
                $word.parents('.cd-words-wrapper').removeClass('is-loading');
                switchWord($word, nextWord);
                setTimeout(function () {
                    hideWord(nextWord);
                }, barAnimationDelay);
                setTimeout(function () {
                    $word.parents('.cd-words-wrapper').addClass('is-loading');
                }, barWaiting);
            } else {
                switchWord($word, nextWord);
                setTimeout(function () {
                    hideWord(nextWord);
                }, animationDelay);
            }
        }

        function showWord($word, $duration) {
            if ($word.parents('.cd-headline').hasClass('type')) {
                showLetter($word.find('i').eq(0), $word, false, $duration);
                $word.addClass('is-visible').removeClass('is-hidden');
            } else if ($word.parents('.cd-headline').hasClass('clip')) {
                $word.parents('.cd-words-wrapper').animate({ 'width': $word.width() + 10 }, revealDuration, function () {
                    setTimeout(function () {
                        hideWord($word);
                    }, revealAnimationDelay);
                });
            }
        }

        function hideLetter($letter, $word, $bool, $duration) {
            $letter.removeClass('in').addClass('out');

            if (!$letter.is(':last-child')) {
                setTimeout(function () {
                    hideLetter($letter.next(), $word, $bool, $duration);
                }, $duration);
            } else if ($bool) {
                setTimeout(function () {
                    hideWord(takeNext($word));
                }, animationDelay);
            }

            if ($letter.is(':last-child') && $('html').hasClass('no-csstransitions')) {
                var nextWord = takeNext($word);
                switchWord($word, nextWord);
            }
        }

        function showLetter($letter, $word, $bool, $duration) {
            $letter.addClass('in').removeClass('out');

            if (!$letter.is(':last-child')) {
                setTimeout(function () {
                    showLetter($letter.next(), $word, $bool, $duration);
                }, $duration);
            } else {
                if ($word.parents('.cd-headline').hasClass('type')) {
                    setTimeout(function () {
                        $word.parents('.cd-words-wrapper').addClass('waiting');
                    }, 200);
                }
                if (!$bool) {
                    setTimeout(function () {
                        hideWord($word);
                    }, animationDelay);
                }
            }
        }

        function takeNext($word) {
            return !$word.is(':last-child') ? $word.next() : $word.parent().children().eq(0);
        }

        function takePrev($word) {
            return !$word.is(':first-child') ? $word.prev() : $word.parent().children().last();
        }

        function switchWord($oldWord, $newWord) {
            $oldWord.removeClass('is-visible').addClass('is-hidden');
            $newWord.removeClass('is-hidden').addClass('is-visible');
        }

<<<<<<< HEAD
        $('.button-collapse').sideNav({
            menuWidth: 240, // Default is 240
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        });

        $('.parallax').parallax();

        var card = document.querySelectorAll('.card-work');
        var transEndEventNames = {
            'WebkitTransition': 'webkitTransitionEnd',
            'MozTransition': 'transitionend',
            'transition': 'transitionend'
        },
            transEndEventName = transEndEventNames[Modernizr.prefixed('transition')];

        function addDashes(name) {
            return name.replace(/([A-Z])/g, function (str, m1) {
                return '-' + m1.toLowerCase();
            });
        }

        function getPopup(id) {
            return document.querySelector('.popup[data-popup="' + id + '"]');
        }

        function getDimensions(el) {
            return el.getBoundingClientRect();
        }

        function getDifference(card, popup) {
            var cardDimensions = getDimensions(card),
                popupDimensions = getDimensions(popup);

            return {
                height: popupDimensions.height / cardDimensions.height,
                width: popupDimensions.width / cardDimensions.width,
                left: popupDimensions.left - cardDimensions.left,
                top: popupDimensions.top - cardDimensions.top
            };
        }

        function transformCard(card, size) {
            return card.style[Modernizr.prefixed('transform')] = 'translate(' + size.left + 'px,' + size.top + 'px)' + ' scale(' + size.width + ',' + size.height + ')';
        }

        function hasClass(elem, cls) {
            var str = " " + elem.className + " ";
            var testCls = " " + cls + " ";
            return str.indexOf(testCls) != -1;
        }

        function closest(e) {
            var el = e.target || e.srcElement;
            if (el = el.parentNode) do {
                //its an inverse loop
                var cls = el.className;
                if (cls) {
                    cls = cls.split(" ");
                    if (-1 !== cls.indexOf("card-work")) {
                        return el;
                        break;
                    }
                }
            } while (el = el.parentNode);
        }

        function scaleCard(e) {
            var el = closest(e);
            var target = el,
                id = target.getAttribute('data-popup-id'),
                popup = getPopup(id);

            var size = getDifference(target, popup);

            target.style[Modernizr.prefixed('transitionDuration')] = '0.5s';
            target.style[Modernizr.prefixed('transitionTimingFunction')] = 'cubic-bezier(0.4, 0, 0.2, 1)';
            target.style[Modernizr.prefixed('transitionProperty')] = addDashes(Modernizr.prefixed('transform'));
            target.style['borderRadius'] = 0;

            transformCard(target, size);
            onAnimated(target, popup);
            onPopupClick(target, popup);
        }

        function onAnimated(card, popup) {
            card.addEventListener(transEndEventName, function transitionEnded() {
                card.style['opacity'] = 0;
                popup.style['visibility'] = 'visible';
                popup.style['zIndex'] = 9999;
                card.removeEventListener(transEndEventName, transitionEnded);
            });
        }

        function onPopupClick(card, popup) {
            popup.addEventListener('click', function toggleVisibility(e) {
                var size = getDifference(popup, card);

                card.style['opacity'] = 1;
                card.style['borderRadius'] = '6px';
                hidePopup(e);
                transformCard(card, size);
            }, false);
        }

        function hidePopup(e) {
            e.target.style['visibility'] = 'hidden';
            e.target.style['zIndex'] = 2;
        }

        // [].forEach.call(card, function(card) {
        // 	card.addEventListener('click', scaleCard, false);
        // });
=======
        // messageFlash
        var messageflash = $(".alert");
        messageflash.delay(3000).slideUp(300);

        // navbar menu
        $('.side-nav').sidenav({
            menuWidth: 240, // Default is 240
            closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
            preventScrolling: false
        }, $('.sidenav-overlay').show());

        // scroll to top
        $('.btn-scrolltop').click(function () {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            return false;
        });
        $('.modal').modal();
        //scroll navigation
        $('.scrollspy').scrollSpy();

        // rendre les images responsive
        $('img').addClass('responsive-img');

        //background navigation on scroll
        $(document).scroll(function () {
            var indexBanner = document.getElementById('index-banner').offsetHeight;
            var scrollTop = $(window).scrollTop();

            if (scrollTop >= indexBanner) {
                $('#nav_home').css('background', '#28282895');
            } else {
                $('#nav_home').css('background', 'transparent');
            }
        });

        // faire apparaitre le bouton scrolltop
        $('#scrolltop').hide();
        $(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 400) {
                    $('#scrolltop').fadeIn(500);
                } else {
                    $('#scrolltop').fadeOut(500);
                }
            });
        });
>>>>>>> dev
    }); // end of document ready
})(jQuery); // end of jQuery name space

/***/ })

/******/ });
<<<<<<< HEAD
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgNDA5ZWQ3ZTNkNmYwNDY5ZGU5ZTMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2FwcC5qcyJdLCJuYW1lcyI6WyIkIiwiYW5pbWF0aW9uRGVsYXkiLCJiYXJBbmltYXRpb25EZWxheSIsImJhcldhaXRpbmciLCJsZXR0ZXJzRGVsYXkiLCJ0eXBlTGV0dGVyc0RlbGF5Iiwic2VsZWN0aW9uRHVyYXRpb24iLCJ0eXBlQW5pbWF0aW9uRGVsYXkiLCJyZXZlYWxEdXJhdGlvbiIsInJldmVhbEFuaW1hdGlvbkRlbGF5IiwiaW5pdEhlYWRsaW5lIiwic2luZ2xlTGV0dGVycyIsImZpbmQiLCJhbmltYXRlSGVhZGxpbmUiLCIkd29yZHMiLCJlYWNoIiwid29yZCIsImxldHRlcnMiLCJ0ZXh0Iiwic3BsaXQiLCJzZWxlY3RlZCIsImhhc0NsYXNzIiwiaSIsInBhcmVudHMiLCJsZW5ndGgiLCJuZXdMZXR0ZXJzIiwiam9pbiIsImh0bWwiLCJjc3MiLCIkaGVhZGxpbmVzIiwiZHVyYXRpb24iLCJoZWFkbGluZSIsInNldFRpbWVvdXQiLCJhZGRDbGFzcyIsInNwYW5XcmFwcGVyIiwibmV3V2lkdGgiLCJ3aWR0aCIsIndvcmRzIiwid29yZFdpZHRoIiwiaGlkZVdvcmQiLCJlcSIsIiR3b3JkIiwibmV4dFdvcmQiLCJ0YWtlTmV4dCIsInBhcmVudFNwYW4iLCJwYXJlbnQiLCJyZW1vdmVDbGFzcyIsImNoaWxkcmVuIiwic2hvd1dvcmQiLCJib29sIiwiaGlkZUxldHRlciIsInNob3dMZXR0ZXIiLCJhbmltYXRlIiwic3dpdGNoV29yZCIsIiRkdXJhdGlvbiIsIiRsZXR0ZXIiLCIkYm9vbCIsImlzIiwibmV4dCIsInRha2VQcmV2IiwicHJldiIsImxhc3QiLCIkb2xkV29yZCIsIiRuZXdXb3JkIiwic2lkZU5hdiIsIm1lbnVXaWR0aCIsImNsb3NlT25DbGljayIsInBhcmFsbGF4IiwiY2FyZCIsImRvY3VtZW50IiwicXVlcnlTZWxlY3RvckFsbCIsInRyYW5zRW5kRXZlbnROYW1lcyIsInRyYW5zRW5kRXZlbnROYW1lIiwiTW9kZXJuaXpyIiwicHJlZml4ZWQiLCJhZGREYXNoZXMiLCJuYW1lIiwicmVwbGFjZSIsInN0ciIsIm0xIiwidG9Mb3dlckNhc2UiLCJnZXRQb3B1cCIsImlkIiwicXVlcnlTZWxlY3RvciIsImdldERpbWVuc2lvbnMiLCJlbCIsImdldEJvdW5kaW5nQ2xpZW50UmVjdCIsImdldERpZmZlcmVuY2UiLCJwb3B1cCIsImNhcmREaW1lbnNpb25zIiwicG9wdXBEaW1lbnNpb25zIiwiaGVpZ2h0IiwibGVmdCIsInRvcCIsInRyYW5zZm9ybUNhcmQiLCJzaXplIiwic3R5bGUiLCJlbGVtIiwiY2xzIiwiY2xhc3NOYW1lIiwidGVzdENscyIsImluZGV4T2YiLCJjbG9zZXN0IiwiZSIsInRhcmdldCIsInNyY0VsZW1lbnQiLCJwYXJlbnROb2RlIiwic2NhbGVDYXJkIiwiZ2V0QXR0cmlidXRlIiwib25BbmltYXRlZCIsIm9uUG9wdXBDbGljayIsImFkZEV2ZW50TGlzdGVuZXIiLCJ0cmFuc2l0aW9uRW5kZWQiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwidG9nZ2xlVmlzaWJpbGl0eSIsImhpZGVQb3B1cCIsImpRdWVyeSJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDN0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsQ0FBQyxVQUFTQSxDQUFULEVBQVc7QUFDUkEsTUFBRSxZQUFVOztBQUVSO0FBQ0E7QUFDQTs7QUFFQTs7QUFFSTtBQUNKLFlBQUlDLGlCQUFpQixJQUFyQjs7QUFDSTtBQUNBQyw0QkFBb0IsSUFGeEI7QUFBQSxZQUdJQyxhQUFhRCxvQkFBb0IsSUFIckM7QUFBQSxZQUcyQztBQUN2QztBQUNBRSx1QkFBZSxFQUxuQjs7QUFNSTtBQUNBQywyQkFBbUIsR0FQdkI7QUFBQSxZQVFJQyxvQkFBb0IsR0FSeEI7QUFBQSxZQVNJQyxxQkFBcUJELG9CQUFvQixHQVQ3Qzs7QUFVSTtBQUNBRSx5QkFBaUIsR0FYckI7QUFBQSxZQVlJQyx1QkFBdUIsSUFaM0I7O0FBY0FDOztBQUdBLGlCQUFTQSxZQUFULEdBQXdCO0FBQ3BCQywwQkFBY1gsRUFBRSxzQkFBRixFQUEwQlksSUFBMUIsQ0FBK0IsR0FBL0IsQ0FBZDtBQUNBQyw0QkFBZ0JiLEVBQUUsY0FBRixDQUFoQjtBQUNIOztBQUVELGlCQUFTVyxhQUFULENBQXVCRyxNQUF2QixFQUErQjtBQUMzQkEsbUJBQU9DLElBQVAsQ0FBWSxZQUFVO0FBQ2xCLG9CQUFJQyxPQUFPaEIsRUFBRSxJQUFGLENBQVg7QUFBQSxvQkFDSWlCLFVBQVVELEtBQUtFLElBQUwsR0FBWUMsS0FBWixDQUFrQixFQUFsQixDQURkO0FBQUEsb0JBRUlDLFdBQVdKLEtBQUtLLFFBQUwsQ0FBYyxZQUFkLENBRmY7QUFHQSxxQkFBS0MsQ0FBTCxJQUFVTCxPQUFWLEVBQW1CO0FBQ2Ysd0JBQUdELEtBQUtPLE9BQUwsQ0FBYSxXQUFiLEVBQTBCQyxNQUExQixHQUFtQyxDQUF0QyxFQUF5Q1AsUUFBUUssQ0FBUixJQUFhLFNBQVNMLFFBQVFLLENBQVIsQ0FBVCxHQUFzQixPQUFuQztBQUN6Q0wsNEJBQVFLLENBQVIsSUFBY0YsUUFBRCxHQUFhLG1CQUFtQkgsUUFBUUssQ0FBUixDQUFuQixHQUFnQyxNQUE3QyxHQUFxRCxRQUFRTCxRQUFRSyxDQUFSLENBQVIsR0FBcUIsTUFBdkY7QUFDSDtBQUNELG9CQUFJRyxhQUFhUixRQUFRUyxJQUFSLENBQWEsRUFBYixDQUFqQjtBQUNBVixxQkFBS1csSUFBTCxDQUFVRixVQUFWLEVBQXNCRyxHQUF0QixDQUEwQixTQUExQixFQUFxQyxDQUFyQztBQUNILGFBVkQ7QUFXSDs7QUFFRCxpQkFBU2YsZUFBVCxDQUF5QmdCLFVBQXpCLEVBQXFDO0FBQ2pDLGdCQUFJQyxXQUFXN0IsY0FBZjtBQUNBNEIsdUJBQVdkLElBQVgsQ0FBZ0IsWUFBVTtBQUN0QixvQkFBSWdCLFdBQVcvQixFQUFFLElBQUYsQ0FBZjs7QUFFQSxvQkFBRytCLFNBQVNWLFFBQVQsQ0FBa0IsYUFBbEIsQ0FBSCxFQUFxQztBQUNqQ1MsK0JBQVc1QixpQkFBWDtBQUNBOEIsK0JBQVcsWUFBVTtBQUFFRCxpQ0FBU25CLElBQVQsQ0FBYyxtQkFBZCxFQUFtQ3FCLFFBQW5DLENBQTRDLFlBQTVDO0FBQTJELHFCQUFsRixFQUFvRjlCLFVBQXBGO0FBQ0gsaUJBSEQsTUFHTyxJQUFJNEIsU0FBU1YsUUFBVCxDQUFrQixNQUFsQixDQUFKLEVBQThCO0FBQ2pDLHdCQUFJYSxjQUFjSCxTQUFTbkIsSUFBVCxDQUFjLG1CQUFkLENBQWxCO0FBQUEsd0JBQ0l1QixXQUFXRCxZQUFZRSxLQUFaLEtBQXNCLEVBRHJDO0FBRUFGLGdDQUFZTixHQUFaLENBQWdCLE9BQWhCLEVBQXlCTyxRQUF6QjtBQUNILGlCQUpNLE1BSUEsSUFBSSxDQUFDSixTQUFTVixRQUFULENBQWtCLE1BQWxCLENBQUwsRUFBaUM7QUFDcEM7QUFDQSx3QkFBSWdCLFFBQVFOLFNBQVNuQixJQUFULENBQWMscUJBQWQsQ0FBWjtBQUFBLHdCQUNJd0IsUUFBUSxDQURaO0FBRUFDLDBCQUFNdEIsSUFBTixDQUFXLFlBQVU7QUFDakIsNEJBQUl1QixZQUFZdEMsRUFBRSxJQUFGLEVBQVFvQyxLQUFSLEVBQWhCO0FBQ0EsNEJBQUlFLFlBQVlGLEtBQWhCLEVBQXVCQSxRQUFRRSxTQUFSO0FBQzFCLHFCQUhEO0FBSUFQLDZCQUFTbkIsSUFBVCxDQUFjLG1CQUFkLEVBQW1DZ0IsR0FBbkMsQ0FBdUMsT0FBdkMsRUFBZ0RRLEtBQWhEO0FBQ0g7O0FBRUQ7QUFDQUosMkJBQVcsWUFBVTtBQUFFTyw2QkFBVVIsU0FBU25CLElBQVQsQ0FBYyxhQUFkLEVBQTZCNEIsRUFBN0IsQ0FBZ0MsQ0FBaEMsQ0FBVjtBQUFnRCxpQkFBdkUsRUFBeUVWLFFBQXpFO0FBQ0gsYUF2QkQ7QUF3Qkg7O0FBRUQsaUJBQVNTLFFBQVQsQ0FBa0JFLEtBQWxCLEVBQXlCO0FBQ3JCLGdCQUFJQyxXQUFXQyxTQUFTRixLQUFULENBQWY7O0FBRUEsZ0JBQUdBLE1BQU1sQixPQUFOLENBQWMsY0FBZCxFQUE4QkYsUUFBOUIsQ0FBdUMsTUFBdkMsQ0FBSCxFQUFtRDtBQUMvQyxvQkFBSXVCLGFBQWFILE1BQU1JLE1BQU4sQ0FBYSxtQkFBYixDQUFqQjtBQUNBRCwyQkFBV1gsUUFBWCxDQUFvQixVQUFwQixFQUFnQ2EsV0FBaEMsQ0FBNEMsU0FBNUM7QUFDQWQsMkJBQVcsWUFBVTtBQUNqQlksK0JBQVdFLFdBQVgsQ0FBdUIsVUFBdkI7QUFDQUwsMEJBQU1LLFdBQU4sQ0FBa0IsWUFBbEIsRUFBZ0NiLFFBQWhDLENBQXlDLFdBQXpDLEVBQXNEYyxRQUF0RCxDQUErRCxHQUEvRCxFQUFvRUQsV0FBcEUsQ0FBZ0YsSUFBaEYsRUFBc0ZiLFFBQXRGLENBQStGLEtBQS9GO0FBQ0gsaUJBSEQsRUFHRzNCLGlCQUhIO0FBSUEwQiwyQkFBVyxZQUFVO0FBQUVnQiw2QkFBU04sUUFBVCxFQUFtQnJDLGdCQUFuQjtBQUFzQyxpQkFBN0QsRUFBK0RFLGtCQUEvRDtBQUVILGFBVEQsTUFTTyxJQUFHa0MsTUFBTWxCLE9BQU4sQ0FBYyxjQUFkLEVBQThCRixRQUE5QixDQUF1QyxTQUF2QyxDQUFILEVBQXNEO0FBQ3pELG9CQUFJNEIsT0FBUVIsTUFBTU0sUUFBTixDQUFlLEdBQWYsRUFBb0J2QixNQUFwQixJQUE4QmtCLFNBQVNLLFFBQVQsQ0FBa0IsR0FBbEIsRUFBdUJ2QixNQUF0RCxHQUFnRSxJQUFoRSxHQUF1RSxLQUFsRjtBQUNBMEIsMkJBQVdULE1BQU03QixJQUFOLENBQVcsR0FBWCxFQUFnQjRCLEVBQWhCLENBQW1CLENBQW5CLENBQVgsRUFBa0NDLEtBQWxDLEVBQXlDUSxJQUF6QyxFQUErQzdDLFlBQS9DO0FBQ0ErQywyQkFBV1QsU0FBUzlCLElBQVQsQ0FBYyxHQUFkLEVBQW1CNEIsRUFBbkIsQ0FBc0IsQ0FBdEIsQ0FBWCxFQUFxQ0UsUUFBckMsRUFBK0NPLElBQS9DLEVBQXFEN0MsWUFBckQ7QUFFSCxhQUxNLE1BS0MsSUFBR3FDLE1BQU1sQixPQUFOLENBQWMsY0FBZCxFQUE4QkYsUUFBOUIsQ0FBdUMsTUFBdkMsQ0FBSCxFQUFtRDtBQUN2RG9CLHNCQUFNbEIsT0FBTixDQUFjLG1CQUFkLEVBQW1DNkIsT0FBbkMsQ0FBMkMsRUFBRWhCLE9BQVEsS0FBVixFQUEzQyxFQUE4RDVCLGNBQTlELEVBQThFLFlBQVU7QUFDcEY2QywrQkFBV1osS0FBWCxFQUFrQkMsUUFBbEI7QUFDQU0sNkJBQVNOLFFBQVQ7QUFDSCxpQkFIRDtBQUtILGFBTk8sTUFNRCxJQUFJRCxNQUFNbEIsT0FBTixDQUFjLGNBQWQsRUFBOEJGLFFBQTlCLENBQXVDLGFBQXZDLENBQUosRUFBMEQ7QUFDN0RvQixzQkFBTWxCLE9BQU4sQ0FBYyxtQkFBZCxFQUFtQ3VCLFdBQW5DLENBQStDLFlBQS9DO0FBQ0FPLDJCQUFXWixLQUFYLEVBQWtCQyxRQUFsQjtBQUNBViwyQkFBVyxZQUFVO0FBQUVPLDZCQUFTRyxRQUFUO0FBQW9CLGlCQUEzQyxFQUE2Q3hDLGlCQUE3QztBQUNBOEIsMkJBQVcsWUFBVTtBQUFFUywwQkFBTWxCLE9BQU4sQ0FBYyxtQkFBZCxFQUFtQ1UsUUFBbkMsQ0FBNEMsWUFBNUM7QUFBMkQsaUJBQWxGLEVBQW9GOUIsVUFBcEY7QUFFSCxhQU5NLE1BTUE7QUFDSGtELDJCQUFXWixLQUFYLEVBQWtCQyxRQUFsQjtBQUNBViwyQkFBVyxZQUFVO0FBQUVPLDZCQUFTRyxRQUFUO0FBQW9CLGlCQUEzQyxFQUE2Q3pDLGNBQTdDO0FBQ0g7QUFDSjs7QUFFRCxpQkFBUytDLFFBQVQsQ0FBa0JQLEtBQWxCLEVBQXlCYSxTQUF6QixFQUFvQztBQUNoQyxnQkFBR2IsTUFBTWxCLE9BQU4sQ0FBYyxjQUFkLEVBQThCRixRQUE5QixDQUF1QyxNQUF2QyxDQUFILEVBQW1EO0FBQy9DOEIsMkJBQVdWLE1BQU03QixJQUFOLENBQVcsR0FBWCxFQUFnQjRCLEVBQWhCLENBQW1CLENBQW5CLENBQVgsRUFBa0NDLEtBQWxDLEVBQXlDLEtBQXpDLEVBQWdEYSxTQUFoRDtBQUNBYixzQkFBTVIsUUFBTixDQUFlLFlBQWYsRUFBNkJhLFdBQTdCLENBQXlDLFdBQXpDO0FBRUgsYUFKRCxNQUlRLElBQUdMLE1BQU1sQixPQUFOLENBQWMsY0FBZCxFQUE4QkYsUUFBOUIsQ0FBdUMsTUFBdkMsQ0FBSCxFQUFtRDtBQUN2RG9CLHNCQUFNbEIsT0FBTixDQUFjLG1CQUFkLEVBQW1DNkIsT0FBbkMsQ0FBMkMsRUFBRSxTQUFVWCxNQUFNTCxLQUFOLEtBQWdCLEVBQTVCLEVBQTNDLEVBQTZFNUIsY0FBN0UsRUFBNkYsWUFBVTtBQUNuR3dCLCtCQUFXLFlBQVU7QUFBRU8saUNBQVNFLEtBQVQ7QUFBaUIscUJBQXhDLEVBQTBDaEMsb0JBQTFDO0FBQ0gsaUJBRkQ7QUFHSDtBQUNKOztBQUVELGlCQUFTeUMsVUFBVCxDQUFvQkssT0FBcEIsRUFBNkJkLEtBQTdCLEVBQW9DZSxLQUFwQyxFQUEyQ0YsU0FBM0MsRUFBc0Q7QUFDbERDLG9CQUFRVCxXQUFSLENBQW9CLElBQXBCLEVBQTBCYixRQUExQixDQUFtQyxLQUFuQzs7QUFFQSxnQkFBRyxDQUFDc0IsUUFBUUUsRUFBUixDQUFXLGFBQVgsQ0FBSixFQUErQjtBQUMzQnpCLDJCQUFXLFlBQVU7QUFBRWtCLCtCQUFXSyxRQUFRRyxJQUFSLEVBQVgsRUFBMkJqQixLQUEzQixFQUFrQ2UsS0FBbEMsRUFBeUNGLFNBQXpDO0FBQXNELGlCQUE3RSxFQUErRUEsU0FBL0U7QUFDSCxhQUZELE1BRU8sSUFBR0UsS0FBSCxFQUFVO0FBQ2J4QiwyQkFBVyxZQUFVO0FBQUVPLDZCQUFTSSxTQUFTRixLQUFULENBQVQ7QUFBMkIsaUJBQWxELEVBQW9EeEMsY0FBcEQ7QUFDSDs7QUFFRCxnQkFBR3NELFFBQVFFLEVBQVIsQ0FBVyxhQUFYLEtBQTZCekQsRUFBRSxNQUFGLEVBQVVxQixRQUFWLENBQW1CLG1CQUFuQixDQUFoQyxFQUF5RTtBQUNyRSxvQkFBSXFCLFdBQVdDLFNBQVNGLEtBQVQsQ0FBZjtBQUNBWSwyQkFBV1osS0FBWCxFQUFrQkMsUUFBbEI7QUFDSDtBQUNKOztBQUVELGlCQUFTUyxVQUFULENBQW9CSSxPQUFwQixFQUE2QmQsS0FBN0IsRUFBb0NlLEtBQXBDLEVBQTJDRixTQUEzQyxFQUFzRDtBQUNsREMsb0JBQVF0QixRQUFSLENBQWlCLElBQWpCLEVBQXVCYSxXQUF2QixDQUFtQyxLQUFuQzs7QUFFQSxnQkFBRyxDQUFDUyxRQUFRRSxFQUFSLENBQVcsYUFBWCxDQUFKLEVBQStCO0FBQzNCekIsMkJBQVcsWUFBVTtBQUFFbUIsK0JBQVdJLFFBQVFHLElBQVIsRUFBWCxFQUEyQmpCLEtBQTNCLEVBQWtDZSxLQUFsQyxFQUF5Q0YsU0FBekM7QUFBc0QsaUJBQTdFLEVBQStFQSxTQUEvRTtBQUNILGFBRkQsTUFFTztBQUNILG9CQUFHYixNQUFNbEIsT0FBTixDQUFjLGNBQWQsRUFBOEJGLFFBQTlCLENBQXVDLE1BQXZDLENBQUgsRUFBbUQ7QUFBRVcsK0JBQVcsWUFBVTtBQUFFUyw4QkFBTWxCLE9BQU4sQ0FBYyxtQkFBZCxFQUFtQ1UsUUFBbkMsQ0FBNEMsU0FBNUM7QUFBeUQscUJBQWhGLEVBQWtGLEdBQWxGO0FBQXdGO0FBQzdJLG9CQUFHLENBQUN1QixLQUFKLEVBQVc7QUFBRXhCLCtCQUFXLFlBQVU7QUFBRU8saUNBQVNFLEtBQVQ7QUFBaUIscUJBQXhDLEVBQTBDeEMsY0FBMUM7QUFBMkQ7QUFDM0U7QUFDSjs7QUFFRCxpQkFBUzBDLFFBQVQsQ0FBa0JGLEtBQWxCLEVBQXlCO0FBQ3JCLG1CQUFRLENBQUNBLE1BQU1nQixFQUFOLENBQVMsYUFBVCxDQUFGLEdBQTZCaEIsTUFBTWlCLElBQU4sRUFBN0IsR0FBNENqQixNQUFNSSxNQUFOLEdBQWVFLFFBQWYsR0FBMEJQLEVBQTFCLENBQTZCLENBQTdCLENBQW5EO0FBQ0g7O0FBRUQsaUJBQVNtQixRQUFULENBQWtCbEIsS0FBbEIsRUFBeUI7QUFDckIsbUJBQVEsQ0FBQ0EsTUFBTWdCLEVBQU4sQ0FBUyxjQUFULENBQUYsR0FBOEJoQixNQUFNbUIsSUFBTixFQUE5QixHQUE2Q25CLE1BQU1JLE1BQU4sR0FBZUUsUUFBZixHQUEwQmMsSUFBMUIsRUFBcEQ7QUFDSDs7QUFFRCxpQkFBU1IsVUFBVCxDQUFvQlMsUUFBcEIsRUFBOEJDLFFBQTlCLEVBQXdDO0FBQ3BDRCxxQkFBU2hCLFdBQVQsQ0FBcUIsWUFBckIsRUFBbUNiLFFBQW5DLENBQTRDLFdBQTVDO0FBQ0E4QixxQkFBU2pCLFdBQVQsQ0FBcUIsV0FBckIsRUFBa0NiLFFBQWxDLENBQTJDLFlBQTNDO0FBQ0g7O0FBRURqQyxVQUFFLGtCQUFGLEVBQXNCZ0UsT0FBdEIsQ0FBOEI7QUFDMUJDLHVCQUFXLEdBRGUsRUFDVjtBQUNoQkMsMEJBQWMsSUFGWSxDQUVQO0FBRk8sU0FBOUI7O0FBS0FsRSxVQUFFLFdBQUYsRUFBZW1FLFFBQWY7O0FBRUEsWUFBSUMsT0FBUUMsU0FBU0MsZ0JBQVQsQ0FBMEIsWUFBMUIsQ0FBWjtBQUNBLFlBQUlDLHFCQUFxQjtBQUNqQixnQ0FBcUIscUJBREo7QUFFakIsNkJBQXFCLGVBRko7QUFHakIsMEJBQXFCO0FBSEosU0FBekI7QUFBQSxZQUtJQyxvQkFBb0JELG1CQUFvQkUsVUFBVUMsUUFBVixDQUFtQixZQUFuQixDQUFwQixDQUx4Qjs7QUFPQSxpQkFBU0MsU0FBVCxDQUFtQkMsSUFBbkIsRUFBeUI7QUFDckIsbUJBQU9BLEtBQUtDLE9BQUwsQ0FBYSxVQUFiLEVBQXlCLFVBQVNDLEdBQVQsRUFBYUMsRUFBYixFQUFnQjtBQUFFLHVCQUFPLE1BQU1BLEdBQUdDLFdBQUgsRUFBYjtBQUFnQyxhQUEzRSxDQUFQO0FBQ0g7O0FBRUQsaUJBQVNDLFFBQVQsQ0FBa0JDLEVBQWxCLEVBQXNCO0FBQ2xCLG1CQUFPYixTQUFTYyxhQUFULENBQXVCLHdCQUF3QkQsRUFBeEIsR0FBNkIsSUFBcEQsQ0FBUDtBQUNIOztBQUVELGlCQUFTRSxhQUFULENBQXVCQyxFQUF2QixFQUEyQjtBQUN2QixtQkFBT0EsR0FBR0MscUJBQUgsRUFBUDtBQUNIOztBQUVELGlCQUFTQyxhQUFULENBQXVCbkIsSUFBdkIsRUFBNkJvQixLQUE3QixFQUFvQztBQUNoQyxnQkFBSUMsaUJBQWlCTCxjQUFjaEIsSUFBZCxDQUFyQjtBQUFBLGdCQUNJc0Isa0JBQWtCTixjQUFjSSxLQUFkLENBRHRCOztBQUdBLG1CQUFPO0FBQ0hHLHdCQUFRRCxnQkFBZ0JDLE1BQWhCLEdBQXlCRixlQUFlRSxNQUQ3QztBQUVIdkQsdUJBQU9zRCxnQkFBZ0J0RCxLQUFoQixHQUF3QnFELGVBQWVyRCxLQUYzQztBQUdId0Qsc0JBQU1GLGdCQUFnQkUsSUFBaEIsR0FBdUJILGVBQWVHLElBSHpDO0FBSUhDLHFCQUFLSCxnQkFBZ0JHLEdBQWhCLEdBQXNCSixlQUFlSTtBQUp2QyxhQUFQO0FBTUg7O0FBRUQsaUJBQVNDLGFBQVQsQ0FBdUIxQixJQUF2QixFQUE2QjJCLElBQTdCLEVBQW1DO0FBQy9CLG1CQUFPM0IsS0FBSzRCLEtBQUwsQ0FBV3ZCLFVBQVVDLFFBQVYsQ0FBbUIsV0FBbkIsQ0FBWCxJQUE4QyxlQUFlcUIsS0FBS0gsSUFBcEIsR0FBMkIsS0FBM0IsR0FBbUNHLEtBQUtGLEdBQXhDLEdBQThDLEtBQTlDLEdBQXNELFNBQXRELEdBQWtFRSxLQUFLM0QsS0FBdkUsR0FBK0UsR0FBL0UsR0FBcUYyRCxLQUFLSixNQUExRixHQUFtRyxHQUF4SjtBQUNIOztBQUVELGlCQUFTdEUsUUFBVCxDQUFrQjRFLElBQWxCLEVBQXdCQyxHQUF4QixFQUE2QjtBQUN6QixnQkFBSXBCLE1BQU0sTUFBTW1CLEtBQUtFLFNBQVgsR0FBdUIsR0FBakM7QUFDQSxnQkFBSUMsVUFBVSxNQUFNRixHQUFOLEdBQVksR0FBMUI7QUFDQSxtQkFBT3BCLElBQUl1QixPQUFKLENBQVlELE9BQVosS0FBd0IsQ0FBQyxDQUFoQztBQUNIOztBQUVELGlCQUFTRSxPQUFULENBQWlCQyxDQUFqQixFQUFvQjtBQUNoQixnQkFBSWxCLEtBQUtrQixFQUFFQyxNQUFGLElBQVlELEVBQUVFLFVBQXZCO0FBQ0EsZ0JBQUlwQixLQUFLQSxHQUFHcUIsVUFBWixFQUF3QixHQUFHO0FBQUU7QUFDekIsb0JBQUlSLE1BQU1iLEdBQUdjLFNBQWI7QUFDQSxvQkFBSUQsR0FBSixFQUFTO0FBQ0xBLDBCQUFNQSxJQUFJL0UsS0FBSixDQUFVLEdBQVYsQ0FBTjtBQUNBLHdCQUFJLENBQUMsQ0FBRCxLQUFPK0UsSUFBSUcsT0FBSixDQUFZLFdBQVosQ0FBWCxFQUFxQztBQUNqQywrQkFBT2hCLEVBQVA7QUFDQTtBQUNIO0FBQ0o7QUFDSixhQVR1QixRQVNmQSxLQUFLQSxHQUFHcUIsVUFUTztBQVUzQjs7QUFFRCxpQkFBU0MsU0FBVCxDQUFtQkosQ0FBbkIsRUFBc0I7QUFDbEIsZ0JBQUlsQixLQUFLaUIsUUFBUUMsQ0FBUixDQUFUO0FBQ0EsZ0JBQUlDLFNBQVNuQixFQUFiO0FBQUEsZ0JBQ0lILEtBQVNzQixPQUFPSSxZQUFQLENBQW9CLGVBQXBCLENBRGI7QUFBQSxnQkFFSXBCLFFBQVNQLFNBQVNDLEVBQVQsQ0FGYjs7QUFJQSxnQkFBSWEsT0FBT1IsY0FBY2lCLE1BQWQsRUFBc0JoQixLQUF0QixDQUFYOztBQUVBZ0IsbUJBQU9SLEtBQVAsQ0FBYXZCLFVBQVVDLFFBQVYsQ0FBbUIsb0JBQW5CLENBQWIsSUFBeUQsTUFBekQ7QUFDQThCLG1CQUFPUixLQUFQLENBQWF2QixVQUFVQyxRQUFWLENBQW1CLDBCQUFuQixDQUFiLElBQStELDhCQUEvRDtBQUNBOEIsbUJBQU9SLEtBQVAsQ0FBYXZCLFVBQVVDLFFBQVYsQ0FBbUIsb0JBQW5CLENBQWIsSUFBeURDLFVBQVVGLFVBQVVDLFFBQVYsQ0FBbUIsV0FBbkIsQ0FBVixDQUF6RDtBQUNBOEIsbUJBQU9SLEtBQVAsQ0FBYSxjQUFiLElBQStCLENBQS9COztBQUVBRiwwQkFBY1UsTUFBZCxFQUFzQlQsSUFBdEI7QUFDQWMsdUJBQVdMLE1BQVgsRUFBbUJoQixLQUFuQjtBQUNBc0IseUJBQWFOLE1BQWIsRUFBcUJoQixLQUFyQjtBQUNIOztBQUVELGlCQUFTcUIsVUFBVCxDQUFvQnpDLElBQXBCLEVBQTBCb0IsS0FBMUIsRUFBaUM7QUFDN0JwQixpQkFBSzJDLGdCQUFMLENBQXNCdkMsaUJBQXRCLEVBQXlDLFNBQVN3QyxlQUFULEdBQTJCO0FBQ2hFNUMscUJBQUs0QixLQUFMLENBQVcsU0FBWCxJQUF3QixDQUF4QjtBQUNBUixzQkFBTVEsS0FBTixDQUFZLFlBQVosSUFBNEIsU0FBNUI7QUFDQVIsc0JBQU1RLEtBQU4sQ0FBWSxRQUFaLElBQXdCLElBQXhCO0FBQ0E1QixxQkFBSzZDLG1CQUFMLENBQXlCekMsaUJBQXpCLEVBQTRDd0MsZUFBNUM7QUFDSCxhQUxEO0FBTUg7O0FBRUQsaUJBQVNGLFlBQVQsQ0FBc0IxQyxJQUF0QixFQUE0Qm9CLEtBQTVCLEVBQW1DO0FBQy9CQSxrQkFBTXVCLGdCQUFOLENBQXVCLE9BQXZCLEVBQWdDLFNBQVNHLGdCQUFULENBQTBCWCxDQUExQixFQUE2QjtBQUN6RCxvQkFBSVIsT0FBT1IsY0FBY0MsS0FBZCxFQUFxQnBCLElBQXJCLENBQVg7O0FBRUFBLHFCQUFLNEIsS0FBTCxDQUFXLFNBQVgsSUFBd0IsQ0FBeEI7QUFDQTVCLHFCQUFLNEIsS0FBTCxDQUFXLGNBQVgsSUFBNkIsS0FBN0I7QUFDQW1CLDBCQUFVWixDQUFWO0FBQ0FULDhCQUFjMUIsSUFBZCxFQUFvQjJCLElBQXBCO0FBQ0gsYUFQRCxFQU9HLEtBUEg7QUFRSDs7QUFHRCxpQkFBU29CLFNBQVQsQ0FBbUJaLENBQW5CLEVBQXNCO0FBQ2xCQSxjQUFFQyxNQUFGLENBQVNSLEtBQVQsQ0FBZSxZQUFmLElBQStCLFFBQS9CO0FBQ0FPLGNBQUVDLE1BQUYsQ0FBU1IsS0FBVCxDQUFlLFFBQWYsSUFBMkIsQ0FBM0I7QUFDSDs7QUFFRDtBQUNBO0FBQ0E7QUFFSCxLQTlRRCxFQURRLENBK1FKO0FBQ1AsQ0FoUkQsRUFnUkdvQixNQWhSSCxFLENBZ1JZLDJCIiwiZmlsZSI6ImpzL2FwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIi9yZXNvdXJjZXMvXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL2Fzc2V0cy9qcy9hcHAuanNcIik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgNDA5ZWQ3ZTNkNmYwNDY5ZGU5ZTMiLCIvLyAvKlxyXG4vLyAgKiBXZWxjb21lIHRvIHlvdXIgYXBwJ3MgbWFpbiBKYXZhU2NyaXB0IGZpbGUhXHJcbi8vICAqXHJcbi8vICAqIFdlIHJlY29tbWVuZCBpbmNsdWRpbmcgdGhlIGJ1aWx0IHZlcnNpb24gb2YgdGhpcyBKYXZhU2NyaXB0IGZpbGVcclxuLy8gICogKGFuZCBpdHMgQ1NTIGZpbGUpIGluIHlvdXIgYmFzZSBsYXlvdXQgKGJhc2UuaHRtbC50d2lnKS5cclxuLy8gICovXHJcbi8vXHJcbi8vIC8vIGFueSBDU1MgeW91IHJlcXVpcmUgd2lsbCBvdXRwdXQgaW50byBhIHNpbmdsZSBjc3MgZmlsZSAoYXBwLmNzcyBpbiB0aGlzIGNhc2UpXHJcbi8vIHJlcXVpcmUoJy4uL2Nzcy9hcHAuY3NzJyk7XHJcbi8vXHJcbi8vIC8vIE5lZWQgalF1ZXJ5PyBJbnN0YWxsIGl0IHdpdGggXCJ5YXJuIGFkZCBqcXVlcnlcIiwgdGhlbiB1bmNvbW1lbnQgdG8gcmVxdWlyZSBpdC5cclxuLy8gLy8gdmFyICQgPSByZXF1aXJlKCdqcXVlcnknKTtcclxuLy9cclxuLy8gY29uc29sZS5sb2coJ0hlbGxvIFdlYnBhY2sgRW5jb3JlISBFZGl0IG1lIGluIGFzc2V0cy9qcy9hcHBfYWRtaW4uanMnKTtcclxuXHJcbihmdW5jdGlvbigkKXtcclxuICAgICQoZnVuY3Rpb24oKXtcclxuXHJcbiAgICAgICAgLy8gJCgnLmJ1dHRvbi1jb2xsYXBzZScpLnNpZGVOYXYoKTtcclxuICAgICAgICAvLyAkKCcuc2Nyb2xsc3B5Jykuc2Nyb2xsU3B5KCk7XHJcbiAgICAgICAgLy8gJChcIi5kcm9wZG93bi10cmlnZ2VyXCIpLmRyb3Bkb3duKCk7XHJcblxyXG4gICAgICAgIC8qKiogQW5pbWF0ZSB3b3JkICoqKi9cclxuXHJcbiAgICAgICAgICAgIC8vc2V0IGFuaW1hdGlvbiB0aW1pbmdcclxuICAgICAgICB2YXIgYW5pbWF0aW9uRGVsYXkgPSAyNTAwLFxyXG4gICAgICAgICAgICAvL2xvYWRpbmcgYmFyIGVmZmVjdFxyXG4gICAgICAgICAgICBiYXJBbmltYXRpb25EZWxheSA9IDM4MDAsXHJcbiAgICAgICAgICAgIGJhcldhaXRpbmcgPSBiYXJBbmltYXRpb25EZWxheSAtIDMwMDAsIC8vMzAwMCBpcyB0aGUgZHVyYXRpb24gb2YgdGhlIHRyYW5zaXRpb24gb24gdGhlIGxvYWRpbmcgYmFyIC0gc2V0IGluIHRoZSBzY3NzL2NzcyBmaWxlXHJcbiAgICAgICAgICAgIC8vbGV0dGVycyBlZmZlY3RcclxuICAgICAgICAgICAgbGV0dGVyc0RlbGF5ID0gNTAsXHJcbiAgICAgICAgICAgIC8vdHlwZSBlZmZlY3RcclxuICAgICAgICAgICAgdHlwZUxldHRlcnNEZWxheSA9IDE1MCxcclxuICAgICAgICAgICAgc2VsZWN0aW9uRHVyYXRpb24gPSA1MDAsXHJcbiAgICAgICAgICAgIHR5cGVBbmltYXRpb25EZWxheSA9IHNlbGVjdGlvbkR1cmF0aW9uICsgODAwLFxyXG4gICAgICAgICAgICAvL2NsaXAgZWZmZWN0XHJcbiAgICAgICAgICAgIHJldmVhbER1cmF0aW9uID0gNjAwLFxyXG4gICAgICAgICAgICByZXZlYWxBbmltYXRpb25EZWxheSA9IDE1MDA7XHJcblxyXG4gICAgICAgIGluaXRIZWFkbGluZSgpO1xyXG5cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gaW5pdEhlYWRsaW5lKCkge1xyXG4gICAgICAgICAgICBzaW5nbGVMZXR0ZXJzKCQoJy5jZC1oZWFkbGluZS5sZXR0ZXJzJykuZmluZCgnYicpKTtcclxuICAgICAgICAgICAgYW5pbWF0ZUhlYWRsaW5lKCQoJy5jZC1oZWFkbGluZScpKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIHNpbmdsZUxldHRlcnMoJHdvcmRzKSB7XHJcbiAgICAgICAgICAgICR3b3Jkcy5lYWNoKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICB2YXIgd29yZCA9ICQodGhpcyksXHJcbiAgICAgICAgICAgICAgICAgICAgbGV0dGVycyA9IHdvcmQudGV4dCgpLnNwbGl0KCcnKSxcclxuICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZCA9IHdvcmQuaGFzQ2xhc3MoJ2lzLXZpc2libGUnKTtcclxuICAgICAgICAgICAgICAgIGZvciAoaSBpbiBsZXR0ZXJzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYod29yZC5wYXJlbnRzKCcucm90YXRlLTInKS5sZW5ndGggPiAwKSBsZXR0ZXJzW2ldID0gJzxlbT4nICsgbGV0dGVyc1tpXSArICc8L2VtPic7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0dGVyc1tpXSA9IChzZWxlY3RlZCkgPyAnPGkgY2xhc3M9XCJpblwiPicgKyBsZXR0ZXJzW2ldICsgJzwvaT4nOiAnPGk+JyArIGxldHRlcnNbaV0gKyAnPC9pPic7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB2YXIgbmV3TGV0dGVycyA9IGxldHRlcnMuam9pbignJyk7XHJcbiAgICAgICAgICAgICAgICB3b3JkLmh0bWwobmV3TGV0dGVycykuY3NzKCdvcGFjaXR5JywgMSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gYW5pbWF0ZUhlYWRsaW5lKCRoZWFkbGluZXMpIHtcclxuICAgICAgICAgICAgdmFyIGR1cmF0aW9uID0gYW5pbWF0aW9uRGVsYXk7XHJcbiAgICAgICAgICAgICRoZWFkbGluZXMuZWFjaChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgdmFyIGhlYWRsaW5lID0gJCh0aGlzKTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZihoZWFkbGluZS5oYXNDbGFzcygnbG9hZGluZy1iYXInKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uID0gYmFyQW5pbWF0aW9uRGVsYXk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpeyBoZWFkbGluZS5maW5kKCcuY2Qtd29yZHMtd3JhcHBlcicpLmFkZENsYXNzKCdpcy1sb2FkaW5nJykgfSwgYmFyV2FpdGluZyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGhlYWRsaW5lLmhhc0NsYXNzKCdjbGlwJykpe1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBzcGFuV3JhcHBlciA9IGhlYWRsaW5lLmZpbmQoJy5jZC13b3Jkcy13cmFwcGVyJyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5ld1dpZHRoID0gc3BhbldyYXBwZXIud2lkdGgoKSArIDEwXHJcbiAgICAgICAgICAgICAgICAgICAgc3BhbldyYXBwZXIuY3NzKCd3aWR0aCcsIG5ld1dpZHRoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoIWhlYWRsaW5lLmhhc0NsYXNzKCd0eXBlJykgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy9hc3NpZ24gdG8gLmNkLXdvcmRzLXdyYXBwZXIgdGhlIHdpZHRoIG9mIGl0cyBsb25nZXN0IHdvcmRcclxuICAgICAgICAgICAgICAgICAgICB2YXIgd29yZHMgPSBoZWFkbGluZS5maW5kKCcuY2Qtd29yZHMtd3JhcHBlciBiJyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoID0gMDtcclxuICAgICAgICAgICAgICAgICAgICB3b3Jkcy5lYWNoKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB3b3JkV2lkdGggPSAkKHRoaXMpLndpZHRoKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh3b3JkV2lkdGggPiB3aWR0aCkgd2lkdGggPSB3b3JkV2lkdGg7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaGVhZGxpbmUuZmluZCgnLmNkLXdvcmRzLXdyYXBwZXInKS5jc3MoJ3dpZHRoJywgd2lkdGgpO1xyXG4gICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICAvL3RyaWdnZXIgYW5pbWF0aW9uXHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7IGhpZGVXb3JkKCBoZWFkbGluZS5maW5kKCcuaXMtdmlzaWJsZScpLmVxKDApICkgfSwgZHVyYXRpb24pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGhpZGVXb3JkKCR3b3JkKSB7XHJcbiAgICAgICAgICAgIHZhciBuZXh0V29yZCA9IHRha2VOZXh0KCR3b3JkKTtcclxuXHJcbiAgICAgICAgICAgIGlmKCR3b3JkLnBhcmVudHMoJy5jZC1oZWFkbGluZScpLmhhc0NsYXNzKCd0eXBlJykpIHtcclxuICAgICAgICAgICAgICAgIHZhciBwYXJlbnRTcGFuID0gJHdvcmQucGFyZW50KCcuY2Qtd29yZHMtd3JhcHBlcicpO1xyXG4gICAgICAgICAgICAgICAgcGFyZW50U3Bhbi5hZGRDbGFzcygnc2VsZWN0ZWQnKS5yZW1vdmVDbGFzcygnd2FpdGluZycpO1xyXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgICAgIHBhcmVudFNwYW4ucmVtb3ZlQ2xhc3MoJ3NlbGVjdGVkJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgJHdvcmQucmVtb3ZlQ2xhc3MoJ2lzLXZpc2libGUnKS5hZGRDbGFzcygnaXMtaGlkZGVuJykuY2hpbGRyZW4oJ2knKS5yZW1vdmVDbGFzcygnaW4nKS5hZGRDbGFzcygnb3V0Jyk7XHJcbiAgICAgICAgICAgICAgICB9LCBzZWxlY3Rpb25EdXJhdGlvbik7XHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7IHNob3dXb3JkKG5leHRXb3JkLCB0eXBlTGV0dGVyc0RlbGF5KSB9LCB0eXBlQW5pbWF0aW9uRGVsYXkpO1xyXG5cclxuICAgICAgICAgICAgfSBlbHNlIGlmKCR3b3JkLnBhcmVudHMoJy5jZC1oZWFkbGluZScpLmhhc0NsYXNzKCdsZXR0ZXJzJykpIHtcclxuICAgICAgICAgICAgICAgIHZhciBib29sID0gKCR3b3JkLmNoaWxkcmVuKCdpJykubGVuZ3RoID49IG5leHRXb3JkLmNoaWxkcmVuKCdpJykubGVuZ3RoKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGhpZGVMZXR0ZXIoJHdvcmQuZmluZCgnaScpLmVxKDApLCAkd29yZCwgYm9vbCwgbGV0dGVyc0RlbGF5KTtcclxuICAgICAgICAgICAgICAgIHNob3dMZXR0ZXIobmV4dFdvcmQuZmluZCgnaScpLmVxKDApLCBuZXh0V29yZCwgYm9vbCwgbGV0dGVyc0RlbGF5KTtcclxuXHJcbiAgICAgICAgICAgIH0gIGVsc2UgaWYoJHdvcmQucGFyZW50cygnLmNkLWhlYWRsaW5lJykuaGFzQ2xhc3MoJ2NsaXAnKSkge1xyXG4gICAgICAgICAgICAgICAgJHdvcmQucGFyZW50cygnLmNkLXdvcmRzLXdyYXBwZXInKS5hbmltYXRlKHsgd2lkdGggOiAnMnB4JyB9LCByZXZlYWxEdXJhdGlvbiwgZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgICAgICBzd2l0Y2hXb3JkKCR3b3JkLCBuZXh0V29yZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2hvd1dvcmQobmV4dFdvcmQpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKCR3b3JkLnBhcmVudHMoJy5jZC1oZWFkbGluZScpLmhhc0NsYXNzKCdsb2FkaW5nLWJhcicpKXtcclxuICAgICAgICAgICAgICAgICR3b3JkLnBhcmVudHMoJy5jZC13b3Jkcy13cmFwcGVyJykucmVtb3ZlQ2xhc3MoJ2lzLWxvYWRpbmcnKTtcclxuICAgICAgICAgICAgICAgIHN3aXRjaFdvcmQoJHdvcmQsIG5leHRXb3JkKTtcclxuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXsgaGlkZVdvcmQobmV4dFdvcmQpIH0sIGJhckFuaW1hdGlvbkRlbGF5KTtcclxuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXsgJHdvcmQucGFyZW50cygnLmNkLXdvcmRzLXdyYXBwZXInKS5hZGRDbGFzcygnaXMtbG9hZGluZycpIH0sIGJhcldhaXRpbmcpO1xyXG5cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHN3aXRjaFdvcmQoJHdvcmQsIG5leHRXb3JkKTtcclxuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXsgaGlkZVdvcmQobmV4dFdvcmQpIH0sIGFuaW1hdGlvbkRlbGF5KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gc2hvd1dvcmQoJHdvcmQsICRkdXJhdGlvbikge1xyXG4gICAgICAgICAgICBpZigkd29yZC5wYXJlbnRzKCcuY2QtaGVhZGxpbmUnKS5oYXNDbGFzcygndHlwZScpKSB7XHJcbiAgICAgICAgICAgICAgICBzaG93TGV0dGVyKCR3b3JkLmZpbmQoJ2knKS5lcSgwKSwgJHdvcmQsIGZhbHNlLCAkZHVyYXRpb24pO1xyXG4gICAgICAgICAgICAgICAgJHdvcmQuYWRkQ2xhc3MoJ2lzLXZpc2libGUnKS5yZW1vdmVDbGFzcygnaXMtaGlkZGVuJyk7XHJcblxyXG4gICAgICAgICAgICB9ICBlbHNlIGlmKCR3b3JkLnBhcmVudHMoJy5jZC1oZWFkbGluZScpLmhhc0NsYXNzKCdjbGlwJykpIHtcclxuICAgICAgICAgICAgICAgICR3b3JkLnBhcmVudHMoJy5jZC13b3Jkcy13cmFwcGVyJykuYW5pbWF0ZSh7ICd3aWR0aCcgOiAkd29yZC53aWR0aCgpICsgMTAgfSwgcmV2ZWFsRHVyYXRpb24sIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpeyBoaWRlV29yZCgkd29yZCkgfSwgcmV2ZWFsQW5pbWF0aW9uRGVsYXkpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGhpZGVMZXR0ZXIoJGxldHRlciwgJHdvcmQsICRib29sLCAkZHVyYXRpb24pIHtcclxuICAgICAgICAgICAgJGxldHRlci5yZW1vdmVDbGFzcygnaW4nKS5hZGRDbGFzcygnb3V0Jyk7XHJcblxyXG4gICAgICAgICAgICBpZighJGxldHRlci5pcygnOmxhc3QtY2hpbGQnKSkge1xyXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpeyBoaWRlTGV0dGVyKCRsZXR0ZXIubmV4dCgpLCAkd29yZCwgJGJvb2wsICRkdXJhdGlvbik7IH0sICRkdXJhdGlvbik7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZigkYm9vbCkge1xyXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpeyBoaWRlV29yZCh0YWtlTmV4dCgkd29yZCkpIH0sIGFuaW1hdGlvbkRlbGF5KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYoJGxldHRlci5pcygnOmxhc3QtY2hpbGQnKSAmJiAkKCdodG1sJykuaGFzQ2xhc3MoJ25vLWNzc3RyYW5zaXRpb25zJykpIHtcclxuICAgICAgICAgICAgICAgIHZhciBuZXh0V29yZCA9IHRha2VOZXh0KCR3b3JkKTtcclxuICAgICAgICAgICAgICAgIHN3aXRjaFdvcmQoJHdvcmQsIG5leHRXb3JkKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gc2hvd0xldHRlcigkbGV0dGVyLCAkd29yZCwgJGJvb2wsICRkdXJhdGlvbikge1xyXG4gICAgICAgICAgICAkbGV0dGVyLmFkZENsYXNzKCdpbicpLnJlbW92ZUNsYXNzKCdvdXQnKTtcclxuXHJcbiAgICAgICAgICAgIGlmKCEkbGV0dGVyLmlzKCc6bGFzdC1jaGlsZCcpKSB7XHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7IHNob3dMZXR0ZXIoJGxldHRlci5uZXh0KCksICR3b3JkLCAkYm9vbCwgJGR1cmF0aW9uKTsgfSwgJGR1cmF0aW9uKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGlmKCR3b3JkLnBhcmVudHMoJy5jZC1oZWFkbGluZScpLmhhc0NsYXNzKCd0eXBlJykpIHsgc2V0VGltZW91dChmdW5jdGlvbigpeyAkd29yZC5wYXJlbnRzKCcuY2Qtd29yZHMtd3JhcHBlcicpLmFkZENsYXNzKCd3YWl0aW5nJyk7IH0sIDIwMCk7fVxyXG4gICAgICAgICAgICAgICAgaWYoISRib29sKSB7IHNldFRpbWVvdXQoZnVuY3Rpb24oKXsgaGlkZVdvcmQoJHdvcmQpIH0sIGFuaW1hdGlvbkRlbGF5KSB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIHRha2VOZXh0KCR3b3JkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAoISR3b3JkLmlzKCc6bGFzdC1jaGlsZCcpKSA/ICR3b3JkLm5leHQoKSA6ICR3b3JkLnBhcmVudCgpLmNoaWxkcmVuKCkuZXEoMCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmdW5jdGlvbiB0YWtlUHJldigkd29yZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gKCEkd29yZC5pcygnOmZpcnN0LWNoaWxkJykpID8gJHdvcmQucHJldigpIDogJHdvcmQucGFyZW50KCkuY2hpbGRyZW4oKS5sYXN0KCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmdW5jdGlvbiBzd2l0Y2hXb3JkKCRvbGRXb3JkLCAkbmV3V29yZCkge1xyXG4gICAgICAgICAgICAkb2xkV29yZC5yZW1vdmVDbGFzcygnaXMtdmlzaWJsZScpLmFkZENsYXNzKCdpcy1oaWRkZW4nKTtcclxuICAgICAgICAgICAgJG5ld1dvcmQucmVtb3ZlQ2xhc3MoJ2lzLWhpZGRlbicpLmFkZENsYXNzKCdpcy12aXNpYmxlJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAkKCcuYnV0dG9uLWNvbGxhcHNlJykuc2lkZU5hdih7XHJcbiAgICAgICAgICAgIG1lbnVXaWR0aDogMjQwLCAvLyBEZWZhdWx0IGlzIDI0MFxyXG4gICAgICAgICAgICBjbG9zZU9uQ2xpY2s6IHRydWUgLy8gQ2xvc2VzIHNpZGUtbmF2IG9uIDxhPiBjbGlja3MsIHVzZWZ1bCBmb3IgQW5ndWxhci9NZXRlb3JcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgJCgnLnBhcmFsbGF4JykucGFyYWxsYXgoKTtcclxuXHJcbiAgICAgICAgdmFyIGNhcmQgID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmNhcmQtd29yaycpO1xyXG4gICAgICAgIHZhciB0cmFuc0VuZEV2ZW50TmFtZXMgPSB7XHJcbiAgICAgICAgICAgICAgICAnV2Via2l0VHJhbnNpdGlvbicgOiAnd2Via2l0VHJhbnNpdGlvbkVuZCcsXHJcbiAgICAgICAgICAgICAgICAnTW96VHJhbnNpdGlvbicgICAgOiAndHJhbnNpdGlvbmVuZCcsXHJcbiAgICAgICAgICAgICAgICAndHJhbnNpdGlvbicgICAgICAgOiAndHJhbnNpdGlvbmVuZCdcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgdHJhbnNFbmRFdmVudE5hbWUgPSB0cmFuc0VuZEV2ZW50TmFtZXNbIE1vZGVybml6ci5wcmVmaXhlZCgndHJhbnNpdGlvbicpIF07XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGFkZERhc2hlcyhuYW1lKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBuYW1lLnJlcGxhY2UoLyhbQS1aXSkvZywgZnVuY3Rpb24oc3RyLG0xKXsgcmV0dXJuICctJyArIG0xLnRvTG93ZXJDYXNlKCk7IH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gZ2V0UG9wdXAoaWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5wb3B1cFtkYXRhLXBvcHVwPVwiJyArIGlkICsgJ1wiXScpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gZ2V0RGltZW5zaW9ucyhlbCkge1xyXG4gICAgICAgICAgICByZXR1cm4gZWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmdW5jdGlvbiBnZXREaWZmZXJlbmNlKGNhcmQsIHBvcHVwKSB7XHJcbiAgICAgICAgICAgIHZhciBjYXJkRGltZW5zaW9ucyA9IGdldERpbWVuc2lvbnMoY2FyZCksXHJcbiAgICAgICAgICAgICAgICBwb3B1cERpbWVuc2lvbnMgPSBnZXREaW1lbnNpb25zKHBvcHVwKTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHBvcHVwRGltZW5zaW9ucy5oZWlnaHQgLyBjYXJkRGltZW5zaW9ucy5oZWlnaHQsXHJcbiAgICAgICAgICAgICAgICB3aWR0aDogcG9wdXBEaW1lbnNpb25zLndpZHRoIC8gY2FyZERpbWVuc2lvbnMud2lkdGgsXHJcbiAgICAgICAgICAgICAgICBsZWZ0OiBwb3B1cERpbWVuc2lvbnMubGVmdCAtIGNhcmREaW1lbnNpb25zLmxlZnQsXHJcbiAgICAgICAgICAgICAgICB0b3A6IHBvcHVwRGltZW5zaW9ucy50b3AgLSBjYXJkRGltZW5zaW9ucy50b3BcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gdHJhbnNmb3JtQ2FyZChjYXJkLCBzaXplKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBjYXJkLnN0eWxlW01vZGVybml6ci5wcmVmaXhlZCgndHJhbnNmb3JtJyldID0gJ3RyYW5zbGF0ZSgnICsgc2l6ZS5sZWZ0ICsgJ3B4LCcgKyBzaXplLnRvcCArICdweCknICsgJyBzY2FsZSgnICsgc2l6ZS53aWR0aCArICcsJyArIHNpemUuaGVpZ2h0ICsgJyknO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gaGFzQ2xhc3MoZWxlbSwgY2xzKSB7XHJcbiAgICAgICAgICAgIHZhciBzdHIgPSBcIiBcIiArIGVsZW0uY2xhc3NOYW1lICsgXCIgXCI7XHJcbiAgICAgICAgICAgIHZhciB0ZXN0Q2xzID0gXCIgXCIgKyBjbHMgKyBcIiBcIjtcclxuICAgICAgICAgICAgcmV0dXJuKHN0ci5pbmRleE9mKHRlc3RDbHMpICE9IC0xKSA7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmdW5jdGlvbiBjbG9zZXN0KGUpIHtcclxuICAgICAgICAgICAgdmFyIGVsID0gZS50YXJnZXQgfHwgZS5zcmNFbGVtZW50O1xyXG4gICAgICAgICAgICBpZiAoZWwgPSBlbC5wYXJlbnROb2RlKSBkbyB7IC8vaXRzIGFuIGludmVyc2UgbG9vcFxyXG4gICAgICAgICAgICAgICAgdmFyIGNscyA9IGVsLmNsYXNzTmFtZTtcclxuICAgICAgICAgICAgICAgIGlmIChjbHMpIHtcclxuICAgICAgICAgICAgICAgICAgICBjbHMgPSBjbHMuc3BsaXQoXCIgXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICgtMSAhPT0gY2xzLmluZGV4T2YoXCJjYXJkLXdvcmtcIikpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGVsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gd2hpbGUgKGVsID0gZWwucGFyZW50Tm9kZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmdW5jdGlvbiBzY2FsZUNhcmQoZSkge1xyXG4gICAgICAgICAgICB2YXIgZWwgPSBjbG9zZXN0KGUpO1xyXG4gICAgICAgICAgICB2YXIgdGFyZ2V0ID0gZWwsXHJcbiAgICAgICAgICAgICAgICBpZCAgICAgPSB0YXJnZXQuZ2V0QXR0cmlidXRlKCdkYXRhLXBvcHVwLWlkJyksXHJcbiAgICAgICAgICAgICAgICBwb3B1cCAgPSBnZXRQb3B1cChpZCk7XHJcblxyXG4gICAgICAgICAgICB2YXIgc2l6ZSA9IGdldERpZmZlcmVuY2UodGFyZ2V0LCBwb3B1cCk7XHJcblxyXG4gICAgICAgICAgICB0YXJnZXQuc3R5bGVbTW9kZXJuaXpyLnByZWZpeGVkKCd0cmFuc2l0aW9uRHVyYXRpb24nKV0gPSAnMC41cyc7XHJcbiAgICAgICAgICAgIHRhcmdldC5zdHlsZVtNb2Rlcm5penIucHJlZml4ZWQoJ3RyYW5zaXRpb25UaW1pbmdGdW5jdGlvbicpXSA9ICdjdWJpYy1iZXppZXIoMC40LCAwLCAwLjIsIDEpJztcclxuICAgICAgICAgICAgdGFyZ2V0LnN0eWxlW01vZGVybml6ci5wcmVmaXhlZCgndHJhbnNpdGlvblByb3BlcnR5JyldID0gYWRkRGFzaGVzKE1vZGVybml6ci5wcmVmaXhlZCgndHJhbnNmb3JtJykpO1xyXG4gICAgICAgICAgICB0YXJnZXQuc3R5bGVbJ2JvcmRlclJhZGl1cyddID0gMDtcclxuXHJcbiAgICAgICAgICAgIHRyYW5zZm9ybUNhcmQodGFyZ2V0LCBzaXplKTtcclxuICAgICAgICAgICAgb25BbmltYXRlZCh0YXJnZXQsIHBvcHVwKTtcclxuICAgICAgICAgICAgb25Qb3B1cENsaWNrKHRhcmdldCwgcG9wdXApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gb25BbmltYXRlZChjYXJkLCBwb3B1cCkge1xyXG4gICAgICAgICAgICBjYXJkLmFkZEV2ZW50TGlzdGVuZXIodHJhbnNFbmRFdmVudE5hbWUsIGZ1bmN0aW9uIHRyYW5zaXRpb25FbmRlZCgpIHtcclxuICAgICAgICAgICAgICAgIGNhcmQuc3R5bGVbJ29wYWNpdHknXSA9IDA7XHJcbiAgICAgICAgICAgICAgICBwb3B1cC5zdHlsZVsndmlzaWJpbGl0eSddID0gJ3Zpc2libGUnO1xyXG4gICAgICAgICAgICAgICAgcG9wdXAuc3R5bGVbJ3pJbmRleCddID0gOTk5OTtcclxuICAgICAgICAgICAgICAgIGNhcmQucmVtb3ZlRXZlbnRMaXN0ZW5lcih0cmFuc0VuZEV2ZW50TmFtZSwgdHJhbnNpdGlvbkVuZGVkKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmdW5jdGlvbiBvblBvcHVwQ2xpY2soY2FyZCwgcG9wdXApIHtcclxuICAgICAgICAgICAgcG9wdXAuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiB0b2dnbGVWaXNpYmlsaXR5KGUpIHtcclxuICAgICAgICAgICAgICAgIHZhciBzaXplID0gZ2V0RGlmZmVyZW5jZShwb3B1cCwgY2FyZCk7XHJcblxyXG4gICAgICAgICAgICAgICAgY2FyZC5zdHlsZVsnb3BhY2l0eSddID0gMTtcclxuICAgICAgICAgICAgICAgIGNhcmQuc3R5bGVbJ2JvcmRlclJhZGl1cyddID0gJzZweCc7XHJcbiAgICAgICAgICAgICAgICBoaWRlUG9wdXAoZSk7XHJcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm1DYXJkKGNhcmQsIHNpemUpO1xyXG4gICAgICAgICAgICB9LCBmYWxzZSk7XHJcbiAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gaGlkZVBvcHVwKGUpIHtcclxuICAgICAgICAgICAgZS50YXJnZXQuc3R5bGVbJ3Zpc2liaWxpdHknXSA9ICdoaWRkZW4nO1xyXG4gICAgICAgICAgICBlLnRhcmdldC5zdHlsZVsnekluZGV4J10gPSAyO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gW10uZm9yRWFjaC5jYWxsKGNhcmQsIGZ1bmN0aW9uKGNhcmQpIHtcclxuICAgICAgICAvLyBcdGNhcmQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBzY2FsZUNhcmQsIGZhbHNlKTtcclxuICAgICAgICAvLyB9KTtcclxuXHJcbiAgICB9KTsgLy8gZW5kIG9mIGRvY3VtZW50IHJlYWR5XHJcbn0pKGpRdWVyeSk7IC8vIGVuZCBvZiBqUXVlcnkgbmFtZSBzcGFjZVxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL2Fzc2V0cy9qcy9hcHAuanMiXSwic291cmNlUm9vdCI6IiJ9
=======
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgNDVjNTRmZjk3MzAxMjMzOWY3MTciLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2FwcC5qcyJdLCJuYW1lcyI6WyIkIiwiYW5pbWF0aW9uRGVsYXkiLCJiYXJBbmltYXRpb25EZWxheSIsImJhcldhaXRpbmciLCJsZXR0ZXJzRGVsYXkiLCJ0eXBlTGV0dGVyc0RlbGF5Iiwic2VsZWN0aW9uRHVyYXRpb24iLCJ0eXBlQW5pbWF0aW9uRGVsYXkiLCJyZXZlYWxEdXJhdGlvbiIsInJldmVhbEFuaW1hdGlvbkRlbGF5IiwiaW5pdEhlYWRsaW5lIiwic2luZ2xlTGV0dGVycyIsImZpbmQiLCJhbmltYXRlSGVhZGxpbmUiLCIkd29yZHMiLCJlYWNoIiwid29yZCIsImxldHRlcnMiLCJ0ZXh0Iiwic3BsaXQiLCJzZWxlY3RlZCIsImhhc0NsYXNzIiwiaSIsInBhcmVudHMiLCJsZW5ndGgiLCJuZXdMZXR0ZXJzIiwiam9pbiIsImh0bWwiLCJjc3MiLCIkaGVhZGxpbmVzIiwiZHVyYXRpb24iLCJoZWFkbGluZSIsInNldFRpbWVvdXQiLCJhZGRDbGFzcyIsInNwYW5XcmFwcGVyIiwibmV3V2lkdGgiLCJ3aWR0aCIsIndvcmRzIiwid29yZFdpZHRoIiwiaGlkZVdvcmQiLCJlcSIsIiR3b3JkIiwibmV4dFdvcmQiLCJ0YWtlTmV4dCIsInBhcmVudFNwYW4iLCJwYXJlbnQiLCJyZW1vdmVDbGFzcyIsImNoaWxkcmVuIiwic2hvd1dvcmQiLCJib29sIiwiaGlkZUxldHRlciIsInNob3dMZXR0ZXIiLCJhbmltYXRlIiwic3dpdGNoV29yZCIsIiRkdXJhdGlvbiIsIiRsZXR0ZXIiLCIkYm9vbCIsImlzIiwibmV4dCIsInRha2VQcmV2IiwicHJldiIsImxhc3QiLCIkb2xkV29yZCIsIiRuZXdXb3JkIiwibWVzc2FnZWZsYXNoIiwiZGVsYXkiLCJzbGlkZVVwIiwic2lkZW5hdiIsIm1lbnVXaWR0aCIsImNsb3NlT25DbGljayIsInByZXZlbnRTY3JvbGxpbmciLCJzaG93IiwiY2xpY2siLCJzY3JvbGxUb3AiLCJtb2RhbCIsInNjcm9sbFNweSIsImRvY3VtZW50Iiwic2Nyb2xsIiwiaW5kZXhCYW5uZXIiLCJnZXRFbGVtZW50QnlJZCIsIm9mZnNldEhlaWdodCIsIndpbmRvdyIsImhpZGUiLCJmYWRlSW4iLCJmYWRlT3V0IiwialF1ZXJ5Il0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUM3REE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMsVUFBU0EsQ0FBVCxFQUFXO0FBQ1JBLE1BQUUsWUFBVTtBQUNSO0FBQ0E7QUFDQSxZQUFJQyxpQkFBaUIsSUFBckI7O0FBQ0k7QUFDQUMsNEJBQW9CLElBRnhCO0FBQUEsWUFHSUMsYUFBYUQsb0JBQW9CLElBSHJDO0FBQUEsWUFHMkM7QUFDdkM7QUFDQUUsdUJBQWUsRUFMbkI7O0FBTUk7QUFDQUMsMkJBQW1CLEdBUHZCO0FBQUEsWUFRSUMsb0JBQW9CLEdBUnhCO0FBQUEsWUFTSUMscUJBQXFCRCxvQkFBb0IsR0FUN0M7O0FBVUk7QUFDQUUseUJBQWlCLEdBWHJCO0FBQUEsWUFZSUMsdUJBQXVCLElBWjNCOztBQWNBQzs7QUFHQSxpQkFBU0EsWUFBVCxHQUF3QjtBQUNwQkMsMEJBQWNYLEVBQUUsc0JBQUYsRUFBMEJZLElBQTFCLENBQStCLEdBQS9CLENBQWQ7QUFDQUMsNEJBQWdCYixFQUFFLGNBQUYsQ0FBaEI7QUFDSDs7QUFFRCxpQkFBU1csYUFBVCxDQUF1QkcsTUFBdkIsRUFBK0I7QUFDM0JBLG1CQUFPQyxJQUFQLENBQVksWUFBVTtBQUNsQixvQkFBSUMsT0FBT2hCLEVBQUUsSUFBRixDQUFYO0FBQUEsb0JBQ0lpQixVQUFVRCxLQUFLRSxJQUFMLEdBQVlDLEtBQVosQ0FBa0IsRUFBbEIsQ0FEZDtBQUFBLG9CQUVJQyxXQUFXSixLQUFLSyxRQUFMLENBQWMsWUFBZCxDQUZmO0FBR0EscUJBQUtDLENBQUwsSUFBVUwsT0FBVixFQUFtQjtBQUNmLHdCQUFHRCxLQUFLTyxPQUFMLENBQWEsV0FBYixFQUEwQkMsTUFBMUIsR0FBbUMsQ0FBdEMsRUFBeUNQLFFBQVFLLENBQVIsSUFBYSxTQUFTTCxRQUFRSyxDQUFSLENBQVQsR0FBc0IsT0FBbkM7QUFDekNMLDRCQUFRSyxDQUFSLElBQWNGLFFBQUQsR0FBYSxtQkFBbUJILFFBQVFLLENBQVIsQ0FBbkIsR0FBZ0MsTUFBN0MsR0FBcUQsUUFBUUwsUUFBUUssQ0FBUixDQUFSLEdBQXFCLE1BQXZGO0FBQ0g7QUFDRCxvQkFBSUcsYUFBYVIsUUFBUVMsSUFBUixDQUFhLEVBQWIsQ0FBakI7QUFDQVYscUJBQUtXLElBQUwsQ0FBVUYsVUFBVixFQUFzQkcsR0FBdEIsQ0FBMEIsU0FBMUIsRUFBcUMsQ0FBckM7QUFDSCxhQVZEO0FBV0g7O0FBRUQsaUJBQVNmLGVBQVQsQ0FBeUJnQixVQUF6QixFQUFxQztBQUNqQyxnQkFBSUMsV0FBVzdCLGNBQWY7QUFDQTRCLHVCQUFXZCxJQUFYLENBQWdCLFlBQVU7QUFDdEIsb0JBQUlnQixXQUFXL0IsRUFBRSxJQUFGLENBQWY7O0FBRUEsb0JBQUcrQixTQUFTVixRQUFULENBQWtCLGFBQWxCLENBQUgsRUFBcUM7QUFDakNTLCtCQUFXNUIsaUJBQVg7QUFDQThCLCtCQUFXLFlBQVU7QUFBRUQsaUNBQVNuQixJQUFULENBQWMsbUJBQWQsRUFBbUNxQixRQUFuQyxDQUE0QyxZQUE1QztBQUEyRCxxQkFBbEYsRUFBb0Y5QixVQUFwRjtBQUNILGlCQUhELE1BR08sSUFBSTRCLFNBQVNWLFFBQVQsQ0FBa0IsTUFBbEIsQ0FBSixFQUE4QjtBQUNqQyx3QkFBSWEsY0FBY0gsU0FBU25CLElBQVQsQ0FBYyxtQkFBZCxDQUFsQjtBQUFBLHdCQUNJdUIsV0FBV0QsWUFBWUUsS0FBWixLQUFzQixFQURyQztBQUVBRixnQ0FBWU4sR0FBWixDQUFnQixPQUFoQixFQUF5Qk8sUUFBekI7QUFDSCxpQkFKTSxNQUlBLElBQUksQ0FBQ0osU0FBU1YsUUFBVCxDQUFrQixNQUFsQixDQUFMLEVBQWlDO0FBQ3BDO0FBQ0Esd0JBQUlnQixRQUFRTixTQUFTbkIsSUFBVCxDQUFjLHFCQUFkLENBQVo7QUFBQSx3QkFDSXdCLFFBQVEsQ0FEWjtBQUVBQywwQkFBTXRCLElBQU4sQ0FBVyxZQUFVO0FBQ2pCLDRCQUFJdUIsWUFBWXRDLEVBQUUsSUFBRixFQUFRb0MsS0FBUixFQUFoQjtBQUNBLDRCQUFJRSxZQUFZRixLQUFoQixFQUF1QkEsUUFBUUUsU0FBUjtBQUMxQixxQkFIRDtBQUlBUCw2QkFBU25CLElBQVQsQ0FBYyxtQkFBZCxFQUFtQ2dCLEdBQW5DLENBQXVDLE9BQXZDLEVBQWdEUSxLQUFoRDtBQUNIOztBQUVEO0FBQ0FKLDJCQUFXLFlBQVU7QUFBRU8sNkJBQVVSLFNBQVNuQixJQUFULENBQWMsYUFBZCxFQUE2QjRCLEVBQTdCLENBQWdDLENBQWhDLENBQVY7QUFBZ0QsaUJBQXZFLEVBQXlFVixRQUF6RTtBQUNILGFBdkJEO0FBd0JIOztBQUVELGlCQUFTUyxRQUFULENBQWtCRSxLQUFsQixFQUF5QjtBQUNyQixnQkFBSUMsV0FBV0MsU0FBU0YsS0FBVCxDQUFmOztBQUVBLGdCQUFHQSxNQUFNbEIsT0FBTixDQUFjLGNBQWQsRUFBOEJGLFFBQTlCLENBQXVDLE1BQXZDLENBQUgsRUFBbUQ7QUFDL0Msb0JBQUl1QixhQUFhSCxNQUFNSSxNQUFOLENBQWEsbUJBQWIsQ0FBakI7QUFDQUQsMkJBQVdYLFFBQVgsQ0FBb0IsVUFBcEIsRUFBZ0NhLFdBQWhDLENBQTRDLFNBQTVDO0FBQ0FkLDJCQUFXLFlBQVU7QUFDakJZLCtCQUFXRSxXQUFYLENBQXVCLFVBQXZCO0FBQ0FMLDBCQUFNSyxXQUFOLENBQWtCLFlBQWxCLEVBQWdDYixRQUFoQyxDQUF5QyxXQUF6QyxFQUFzRGMsUUFBdEQsQ0FBK0QsR0FBL0QsRUFBb0VELFdBQXBFLENBQWdGLElBQWhGLEVBQXNGYixRQUF0RixDQUErRixLQUEvRjtBQUNILGlCQUhELEVBR0czQixpQkFISDtBQUlBMEIsMkJBQVcsWUFBVTtBQUFFZ0IsNkJBQVNOLFFBQVQsRUFBbUJyQyxnQkFBbkI7QUFBc0MsaUJBQTdELEVBQStERSxrQkFBL0Q7QUFFSCxhQVRELE1BU08sSUFBR2tDLE1BQU1sQixPQUFOLENBQWMsY0FBZCxFQUE4QkYsUUFBOUIsQ0FBdUMsU0FBdkMsQ0FBSCxFQUFzRDtBQUN6RCxvQkFBSTRCLE9BQVFSLE1BQU1NLFFBQU4sQ0FBZSxHQUFmLEVBQW9CdkIsTUFBcEIsSUFBOEJrQixTQUFTSyxRQUFULENBQWtCLEdBQWxCLEVBQXVCdkIsTUFBdEQsR0FBZ0UsSUFBaEUsR0FBdUUsS0FBbEY7QUFDQTBCLDJCQUFXVCxNQUFNN0IsSUFBTixDQUFXLEdBQVgsRUFBZ0I0QixFQUFoQixDQUFtQixDQUFuQixDQUFYLEVBQWtDQyxLQUFsQyxFQUF5Q1EsSUFBekMsRUFBK0M3QyxZQUEvQztBQUNBK0MsMkJBQVdULFNBQVM5QixJQUFULENBQWMsR0FBZCxFQUFtQjRCLEVBQW5CLENBQXNCLENBQXRCLENBQVgsRUFBcUNFLFFBQXJDLEVBQStDTyxJQUEvQyxFQUFxRDdDLFlBQXJEO0FBRUgsYUFMTSxNQUtDLElBQUdxQyxNQUFNbEIsT0FBTixDQUFjLGNBQWQsRUFBOEJGLFFBQTlCLENBQXVDLE1BQXZDLENBQUgsRUFBbUQ7QUFDdkRvQixzQkFBTWxCLE9BQU4sQ0FBYyxtQkFBZCxFQUFtQzZCLE9BQW5DLENBQTJDLEVBQUVoQixPQUFRLEtBQVYsRUFBM0MsRUFBOEQ1QixjQUE5RCxFQUE4RSxZQUFVO0FBQ3BGNkMsK0JBQVdaLEtBQVgsRUFBa0JDLFFBQWxCO0FBQ0FNLDZCQUFTTixRQUFUO0FBQ0gsaUJBSEQ7QUFLSCxhQU5PLE1BTUQsSUFBSUQsTUFBTWxCLE9BQU4sQ0FBYyxjQUFkLEVBQThCRixRQUE5QixDQUF1QyxhQUF2QyxDQUFKLEVBQTBEO0FBQzdEb0Isc0JBQU1sQixPQUFOLENBQWMsbUJBQWQsRUFBbUN1QixXQUFuQyxDQUErQyxZQUEvQztBQUNBTywyQkFBV1osS0FBWCxFQUFrQkMsUUFBbEI7QUFDQVYsMkJBQVcsWUFBVTtBQUFFTyw2QkFBU0csUUFBVDtBQUFvQixpQkFBM0MsRUFBNkN4QyxpQkFBN0M7QUFDQThCLDJCQUFXLFlBQVU7QUFBRVMsMEJBQU1sQixPQUFOLENBQWMsbUJBQWQsRUFBbUNVLFFBQW5DLENBQTRDLFlBQTVDO0FBQTJELGlCQUFsRixFQUFvRjlCLFVBQXBGO0FBRUgsYUFOTSxNQU1BO0FBQ0hrRCwyQkFBV1osS0FBWCxFQUFrQkMsUUFBbEI7QUFDQVYsMkJBQVcsWUFBVTtBQUFFTyw2QkFBU0csUUFBVDtBQUFvQixpQkFBM0MsRUFBNkN6QyxjQUE3QztBQUNIO0FBQ0o7O0FBRUQsaUJBQVMrQyxRQUFULENBQWtCUCxLQUFsQixFQUF5QmEsU0FBekIsRUFBb0M7QUFDaEMsZ0JBQUdiLE1BQU1sQixPQUFOLENBQWMsY0FBZCxFQUE4QkYsUUFBOUIsQ0FBdUMsTUFBdkMsQ0FBSCxFQUFtRDtBQUMvQzhCLDJCQUFXVixNQUFNN0IsSUFBTixDQUFXLEdBQVgsRUFBZ0I0QixFQUFoQixDQUFtQixDQUFuQixDQUFYLEVBQWtDQyxLQUFsQyxFQUF5QyxLQUF6QyxFQUFnRGEsU0FBaEQ7QUFDQWIsc0JBQU1SLFFBQU4sQ0FBZSxZQUFmLEVBQTZCYSxXQUE3QixDQUF5QyxXQUF6QztBQUVILGFBSkQsTUFJUSxJQUFHTCxNQUFNbEIsT0FBTixDQUFjLGNBQWQsRUFBOEJGLFFBQTlCLENBQXVDLE1BQXZDLENBQUgsRUFBbUQ7QUFDdkRvQixzQkFBTWxCLE9BQU4sQ0FBYyxtQkFBZCxFQUFtQzZCLE9BQW5DLENBQTJDLEVBQUUsU0FBVVgsTUFBTUwsS0FBTixLQUFnQixFQUE1QixFQUEzQyxFQUE2RTVCLGNBQTdFLEVBQTZGLFlBQVU7QUFDbkd3QiwrQkFBVyxZQUFVO0FBQUVPLGlDQUFTRSxLQUFUO0FBQWlCLHFCQUF4QyxFQUEwQ2hDLG9CQUExQztBQUNILGlCQUZEO0FBR0g7QUFDSjs7QUFFRCxpQkFBU3lDLFVBQVQsQ0FBb0JLLE9BQXBCLEVBQTZCZCxLQUE3QixFQUFvQ2UsS0FBcEMsRUFBMkNGLFNBQTNDLEVBQXNEO0FBQ2xEQyxvQkFBUVQsV0FBUixDQUFvQixJQUFwQixFQUEwQmIsUUFBMUIsQ0FBbUMsS0FBbkM7O0FBRUEsZ0JBQUcsQ0FBQ3NCLFFBQVFFLEVBQVIsQ0FBVyxhQUFYLENBQUosRUFBK0I7QUFDM0J6QiwyQkFBVyxZQUFVO0FBQUVrQiwrQkFBV0ssUUFBUUcsSUFBUixFQUFYLEVBQTJCakIsS0FBM0IsRUFBa0NlLEtBQWxDLEVBQXlDRixTQUF6QztBQUFzRCxpQkFBN0UsRUFBK0VBLFNBQS9FO0FBQ0gsYUFGRCxNQUVPLElBQUdFLEtBQUgsRUFBVTtBQUNieEIsMkJBQVcsWUFBVTtBQUFFTyw2QkFBU0ksU0FBU0YsS0FBVCxDQUFUO0FBQTJCLGlCQUFsRCxFQUFvRHhDLGNBQXBEO0FBQ0g7O0FBRUQsZ0JBQUdzRCxRQUFRRSxFQUFSLENBQVcsYUFBWCxLQUE2QnpELEVBQUUsTUFBRixFQUFVcUIsUUFBVixDQUFtQixtQkFBbkIsQ0FBaEMsRUFBeUU7QUFDckUsb0JBQUlxQixXQUFXQyxTQUFTRixLQUFULENBQWY7QUFDQVksMkJBQVdaLEtBQVgsRUFBa0JDLFFBQWxCO0FBQ0g7QUFDSjs7QUFFRCxpQkFBU1MsVUFBVCxDQUFvQkksT0FBcEIsRUFBNkJkLEtBQTdCLEVBQW9DZSxLQUFwQyxFQUEyQ0YsU0FBM0MsRUFBc0Q7QUFDbERDLG9CQUFRdEIsUUFBUixDQUFpQixJQUFqQixFQUF1QmEsV0FBdkIsQ0FBbUMsS0FBbkM7O0FBRUEsZ0JBQUcsQ0FBQ1MsUUFBUUUsRUFBUixDQUFXLGFBQVgsQ0FBSixFQUErQjtBQUMzQnpCLDJCQUFXLFlBQVU7QUFBRW1CLCtCQUFXSSxRQUFRRyxJQUFSLEVBQVgsRUFBMkJqQixLQUEzQixFQUFrQ2UsS0FBbEMsRUFBeUNGLFNBQXpDO0FBQXNELGlCQUE3RSxFQUErRUEsU0FBL0U7QUFDSCxhQUZELE1BRU87QUFDSCxvQkFBR2IsTUFBTWxCLE9BQU4sQ0FBYyxjQUFkLEVBQThCRixRQUE5QixDQUF1QyxNQUF2QyxDQUFILEVBQW1EO0FBQUVXLCtCQUFXLFlBQVU7QUFBRVMsOEJBQU1sQixPQUFOLENBQWMsbUJBQWQsRUFBbUNVLFFBQW5DLENBQTRDLFNBQTVDO0FBQXlELHFCQUFoRixFQUFrRixHQUFsRjtBQUF3RjtBQUM3SSxvQkFBRyxDQUFDdUIsS0FBSixFQUFXO0FBQUV4QiwrQkFBVyxZQUFVO0FBQUVPLGlDQUFTRSxLQUFUO0FBQWlCLHFCQUF4QyxFQUEwQ3hDLGNBQTFDO0FBQTJEO0FBQzNFO0FBQ0o7O0FBRUQsaUJBQVMwQyxRQUFULENBQWtCRixLQUFsQixFQUF5QjtBQUNyQixtQkFBUSxDQUFDQSxNQUFNZ0IsRUFBTixDQUFTLGFBQVQsQ0FBRixHQUE2QmhCLE1BQU1pQixJQUFOLEVBQTdCLEdBQTRDakIsTUFBTUksTUFBTixHQUFlRSxRQUFmLEdBQTBCUCxFQUExQixDQUE2QixDQUE3QixDQUFuRDtBQUNIOztBQUVELGlCQUFTbUIsUUFBVCxDQUFrQmxCLEtBQWxCLEVBQXlCO0FBQ3JCLG1CQUFRLENBQUNBLE1BQU1nQixFQUFOLENBQVMsY0FBVCxDQUFGLEdBQThCaEIsTUFBTW1CLElBQU4sRUFBOUIsR0FBNkNuQixNQUFNSSxNQUFOLEdBQWVFLFFBQWYsR0FBMEJjLElBQTFCLEVBQXBEO0FBQ0g7O0FBRUQsaUJBQVNSLFVBQVQsQ0FBb0JTLFFBQXBCLEVBQThCQyxRQUE5QixFQUF3QztBQUNwQ0QscUJBQVNoQixXQUFULENBQXFCLFlBQXJCLEVBQW1DYixRQUFuQyxDQUE0QyxXQUE1QztBQUNBOEIscUJBQVNqQixXQUFULENBQXFCLFdBQXJCLEVBQWtDYixRQUFsQyxDQUEyQyxZQUEzQztBQUNIOztBQUVEO0FBQ0EsWUFBSStCLGVBQWVoRSxFQUFFLFFBQUYsQ0FBbkI7QUFDQWdFLHFCQUFhQyxLQUFiLENBQW1CLElBQW5CLEVBQXlCQyxPQUF6QixDQUFpQyxHQUFqQzs7QUFFQTtBQUNBbEUsVUFBRSxXQUFGLEVBQWVtRSxPQUFmLENBQ0k7QUFDSUMsdUJBQVcsR0FEZixFQUNvQjtBQUNoQkMsMEJBQWMsSUFGbEIsRUFFd0I7QUFDcEJDLDhCQUFrQjtBQUh0QixTQURKLEVBTUl0RSxFQUFFLGtCQUFGLEVBQXNCdUUsSUFBdEIsRUFOSjs7QUFTQTtBQUNBdkUsVUFBRSxnQkFBRixFQUFvQndFLEtBQXBCLENBQTBCLFlBQVc7QUFDakN4RSxjQUFFLFlBQUYsRUFBZ0JvRCxPQUFoQixDQUF3QixFQUFFcUIsV0FBVyxDQUFiLEVBQXhCLEVBQTBDLE1BQTFDO0FBQ0EsbUJBQU8sS0FBUDtBQUNILFNBSEQ7QUFJQXpFLFVBQUUsUUFBRixFQUFZMEUsS0FBWjtBQUNBO0FBQ0ExRSxVQUFFLFlBQUYsRUFBZ0IyRSxTQUFoQjs7QUFFQTtBQUNBM0UsVUFBRSxLQUFGLEVBQVNpQyxRQUFULENBQWtCLGdCQUFsQjs7QUFFQTtBQUNBakMsVUFBRTRFLFFBQUYsRUFBWUMsTUFBWixDQUFtQixZQUFXO0FBQzFCLGdCQUFJQyxjQUFjRixTQUFTRyxjQUFULENBQXdCLGNBQXhCLEVBQXdDQyxZQUExRDtBQUNBLGdCQUFJUCxZQUFZekUsRUFBRWlGLE1BQUYsRUFBVVIsU0FBVixFQUFoQjs7QUFFQSxnQkFBSUEsYUFBYUssV0FBakIsRUFBOEI7QUFDMUI5RSxrQkFBRSxXQUFGLEVBQWU0QixHQUFmLENBQW1CLFlBQW5CLEVBQWlDLFdBQWpDO0FBQ0gsYUFGRCxNQUVNO0FBQ0Y1QixrQkFBRSxXQUFGLEVBQWU0QixHQUFmLENBQW1CLFlBQW5CLEVBQWlDLGFBQWpDO0FBQ0g7QUFDSixTQVREOztBQVdBO0FBQ0E1QixVQUFFLFlBQUYsRUFBZ0JrRixJQUFoQjtBQUNBbEYsVUFBRSxZQUFZO0FBQ1ZBLGNBQUVpRixNQUFGLEVBQVVKLE1BQVYsQ0FBaUIsWUFBWTtBQUN6QixvQkFBSTdFLEVBQUUsSUFBRixFQUFReUUsU0FBUixLQUFzQixHQUExQixFQUFnQztBQUM1QnpFLHNCQUFFLFlBQUYsRUFBZ0JtRixNQUFoQixDQUF1QixHQUF2QjtBQUNILGlCQUZELE1BRU87QUFDSG5GLHNCQUFFLFlBQUYsRUFBZ0JvRixPQUFoQixDQUF3QixHQUF4QjtBQUNIO0FBRUosYUFQRDtBQVFILFNBVEQ7QUFXSCxLQTVNRCxFQURRLENBNk1KO0FBQ1AsQ0E5TUQsRUE4TUdDLE1BOU1ILEUsQ0E4TVksMkIiLCJmaWxlIjoianMvYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiL3Jlc291cmNlcy9cIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vYXNzZXRzL2pzL2FwcC5qc1wiKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCA0NWM1NGZmOTczMDEyMzM5ZjcxNyIsIi8vIC8qXHJcbi8vICAqIFdlbGNvbWUgdG8geW91ciBhcHAncyBtYWluIEphdmFTY3JpcHQgZmlsZSFcclxuLy8gICpcclxuLy8gICogV2UgcmVjb21tZW5kIGluY2x1ZGluZyB0aGUgYnVpbHQgdmVyc2lvbiBvZiB0aGlzIEphdmFTY3JpcHQgZmlsZVxyXG4vLyAgKiAoYW5kIGl0cyBDU1MgZmlsZSkgaW4geW91ciBiYXNlIGxheW91dCAoYmFzZS5odG1sLnR3aWcpLlxyXG4vLyAgKi9cclxuLy9cclxuLy8gLy8gYW55IENTUyB5b3UgcmVxdWlyZSB3aWxsIG91dHB1dCBpbnRvIGEgc2luZ2xlIGNzcyBmaWxlIChhcHAuY3NzIGluIHRoaXMgY2FzZSlcclxuLy8gcmVxdWlyZSgnLi4vY3NzL2FwcC5jc3MnKTtcclxuLy9cclxuLy8gLy8gTmVlZCBqUXVlcnk/IEluc3RhbGwgaXQgd2l0aCBcInlhcm4gYWRkIGpxdWVyeVwiLCB0aGVuIHVuY29tbWVudCB0byByZXF1aXJlIGl0LlxyXG4vLyAvLyB2YXIgJCA9IHJlcXVpcmUoJ2pxdWVyeScpO1xyXG4vL1xyXG4vLyBjb25zb2xlLmxvZygnSGVsbG8gV2VicGFjayBFbmNvcmUhIEVkaXQgbWUgaW4gYXNzZXRzL2pzL2FwcF9hZG1pbi5qcycpO1xyXG4oZnVuY3Rpb24oJCl7XHJcbiAgICAkKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgLyoqKiBBbmltYXRlIHdvcmQgKioqL1xyXG4gICAgICAgIC8vc2V0IGFuaW1hdGlvbiB0aW1pbmdcclxuICAgICAgICB2YXIgYW5pbWF0aW9uRGVsYXkgPSAyNTAwLFxyXG4gICAgICAgICAgICAvL2xvYWRpbmcgYmFyIGVmZmVjdFxyXG4gICAgICAgICAgICBiYXJBbmltYXRpb25EZWxheSA9IDM4MDAsXHJcbiAgICAgICAgICAgIGJhcldhaXRpbmcgPSBiYXJBbmltYXRpb25EZWxheSAtIDMwMDAsIC8vMzAwMCBpcyB0aGUgZHVyYXRpb24gb2YgdGhlIHRyYW5zaXRpb24gb24gdGhlIGxvYWRpbmcgYmFyIC0gc2V0IGluIHRoZSBzY3NzL2NzcyBmaWxlXHJcbiAgICAgICAgICAgIC8vbGV0dGVycyBlZmZlY3RcclxuICAgICAgICAgICAgbGV0dGVyc0RlbGF5ID0gNTAsXHJcbiAgICAgICAgICAgIC8vdHlwZSBlZmZlY3RcclxuICAgICAgICAgICAgdHlwZUxldHRlcnNEZWxheSA9IDE1MCxcclxuICAgICAgICAgICAgc2VsZWN0aW9uRHVyYXRpb24gPSA1MDAsXHJcbiAgICAgICAgICAgIHR5cGVBbmltYXRpb25EZWxheSA9IHNlbGVjdGlvbkR1cmF0aW9uICsgODAwLFxyXG4gICAgICAgICAgICAvL2NsaXAgZWZmZWN0XHJcbiAgICAgICAgICAgIHJldmVhbER1cmF0aW9uID0gNjAwLFxyXG4gICAgICAgICAgICByZXZlYWxBbmltYXRpb25EZWxheSA9IDE1MDA7XHJcblxyXG4gICAgICAgIGluaXRIZWFkbGluZSgpO1xyXG5cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gaW5pdEhlYWRsaW5lKCkge1xyXG4gICAgICAgICAgICBzaW5nbGVMZXR0ZXJzKCQoJy5jZC1oZWFkbGluZS5sZXR0ZXJzJykuZmluZCgnYicpKTtcclxuICAgICAgICAgICAgYW5pbWF0ZUhlYWRsaW5lKCQoJy5jZC1oZWFkbGluZScpKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIHNpbmdsZUxldHRlcnMoJHdvcmRzKSB7XHJcbiAgICAgICAgICAgICR3b3Jkcy5lYWNoKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICB2YXIgd29yZCA9ICQodGhpcyksXHJcbiAgICAgICAgICAgICAgICAgICAgbGV0dGVycyA9IHdvcmQudGV4dCgpLnNwbGl0KCcnKSxcclxuICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZCA9IHdvcmQuaGFzQ2xhc3MoJ2lzLXZpc2libGUnKTtcclxuICAgICAgICAgICAgICAgIGZvciAoaSBpbiBsZXR0ZXJzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYod29yZC5wYXJlbnRzKCcucm90YXRlLTInKS5sZW5ndGggPiAwKSBsZXR0ZXJzW2ldID0gJzxlbT4nICsgbGV0dGVyc1tpXSArICc8L2VtPic7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0dGVyc1tpXSA9IChzZWxlY3RlZCkgPyAnPGkgY2xhc3M9XCJpblwiPicgKyBsZXR0ZXJzW2ldICsgJzwvaT4nOiAnPGk+JyArIGxldHRlcnNbaV0gKyAnPC9pPic7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB2YXIgbmV3TGV0dGVycyA9IGxldHRlcnMuam9pbignJyk7XHJcbiAgICAgICAgICAgICAgICB3b3JkLmh0bWwobmV3TGV0dGVycykuY3NzKCdvcGFjaXR5JywgMSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gYW5pbWF0ZUhlYWRsaW5lKCRoZWFkbGluZXMpIHtcclxuICAgICAgICAgICAgdmFyIGR1cmF0aW9uID0gYW5pbWF0aW9uRGVsYXk7XHJcbiAgICAgICAgICAgICRoZWFkbGluZXMuZWFjaChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgdmFyIGhlYWRsaW5lID0gJCh0aGlzKTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZihoZWFkbGluZS5oYXNDbGFzcygnbG9hZGluZy1iYXInKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uID0gYmFyQW5pbWF0aW9uRGVsYXk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpeyBoZWFkbGluZS5maW5kKCcuY2Qtd29yZHMtd3JhcHBlcicpLmFkZENsYXNzKCdpcy1sb2FkaW5nJykgfSwgYmFyV2FpdGluZyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGhlYWRsaW5lLmhhc0NsYXNzKCdjbGlwJykpe1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBzcGFuV3JhcHBlciA9IGhlYWRsaW5lLmZpbmQoJy5jZC13b3Jkcy13cmFwcGVyJyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5ld1dpZHRoID0gc3BhbldyYXBwZXIud2lkdGgoKSArIDEwXHJcbiAgICAgICAgICAgICAgICAgICAgc3BhbldyYXBwZXIuY3NzKCd3aWR0aCcsIG5ld1dpZHRoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoIWhlYWRsaW5lLmhhc0NsYXNzKCd0eXBlJykgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy9hc3NpZ24gdG8gLmNkLXdvcmRzLXdyYXBwZXIgdGhlIHdpZHRoIG9mIGl0cyBsb25nZXN0IHdvcmRcclxuICAgICAgICAgICAgICAgICAgICB2YXIgd29yZHMgPSBoZWFkbGluZS5maW5kKCcuY2Qtd29yZHMtd3JhcHBlciBiJyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoID0gMDtcclxuICAgICAgICAgICAgICAgICAgICB3b3Jkcy5lYWNoKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB3b3JkV2lkdGggPSAkKHRoaXMpLndpZHRoKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh3b3JkV2lkdGggPiB3aWR0aCkgd2lkdGggPSB3b3JkV2lkdGg7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaGVhZGxpbmUuZmluZCgnLmNkLXdvcmRzLXdyYXBwZXInKS5jc3MoJ3dpZHRoJywgd2lkdGgpO1xyXG4gICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICAvL3RyaWdnZXIgYW5pbWF0aW9uXHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7IGhpZGVXb3JkKCBoZWFkbGluZS5maW5kKCcuaXMtdmlzaWJsZScpLmVxKDApICkgfSwgZHVyYXRpb24pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGhpZGVXb3JkKCR3b3JkKSB7XHJcbiAgICAgICAgICAgIHZhciBuZXh0V29yZCA9IHRha2VOZXh0KCR3b3JkKTtcclxuXHJcbiAgICAgICAgICAgIGlmKCR3b3JkLnBhcmVudHMoJy5jZC1oZWFkbGluZScpLmhhc0NsYXNzKCd0eXBlJykpIHtcclxuICAgICAgICAgICAgICAgIHZhciBwYXJlbnRTcGFuID0gJHdvcmQucGFyZW50KCcuY2Qtd29yZHMtd3JhcHBlcicpO1xyXG4gICAgICAgICAgICAgICAgcGFyZW50U3Bhbi5hZGRDbGFzcygnc2VsZWN0ZWQnKS5yZW1vdmVDbGFzcygnd2FpdGluZycpO1xyXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgICAgIHBhcmVudFNwYW4ucmVtb3ZlQ2xhc3MoJ3NlbGVjdGVkJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgJHdvcmQucmVtb3ZlQ2xhc3MoJ2lzLXZpc2libGUnKS5hZGRDbGFzcygnaXMtaGlkZGVuJykuY2hpbGRyZW4oJ2knKS5yZW1vdmVDbGFzcygnaW4nKS5hZGRDbGFzcygnb3V0Jyk7XHJcbiAgICAgICAgICAgICAgICB9LCBzZWxlY3Rpb25EdXJhdGlvbik7XHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7IHNob3dXb3JkKG5leHRXb3JkLCB0eXBlTGV0dGVyc0RlbGF5KSB9LCB0eXBlQW5pbWF0aW9uRGVsYXkpO1xyXG5cclxuICAgICAgICAgICAgfSBlbHNlIGlmKCR3b3JkLnBhcmVudHMoJy5jZC1oZWFkbGluZScpLmhhc0NsYXNzKCdsZXR0ZXJzJykpIHtcclxuICAgICAgICAgICAgICAgIHZhciBib29sID0gKCR3b3JkLmNoaWxkcmVuKCdpJykubGVuZ3RoID49IG5leHRXb3JkLmNoaWxkcmVuKCdpJykubGVuZ3RoKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGhpZGVMZXR0ZXIoJHdvcmQuZmluZCgnaScpLmVxKDApLCAkd29yZCwgYm9vbCwgbGV0dGVyc0RlbGF5KTtcclxuICAgICAgICAgICAgICAgIHNob3dMZXR0ZXIobmV4dFdvcmQuZmluZCgnaScpLmVxKDApLCBuZXh0V29yZCwgYm9vbCwgbGV0dGVyc0RlbGF5KTtcclxuXHJcbiAgICAgICAgICAgIH0gIGVsc2UgaWYoJHdvcmQucGFyZW50cygnLmNkLWhlYWRsaW5lJykuaGFzQ2xhc3MoJ2NsaXAnKSkge1xyXG4gICAgICAgICAgICAgICAgJHdvcmQucGFyZW50cygnLmNkLXdvcmRzLXdyYXBwZXInKS5hbmltYXRlKHsgd2lkdGggOiAnMnB4JyB9LCByZXZlYWxEdXJhdGlvbiwgZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgICAgICBzd2l0Y2hXb3JkKCR3b3JkLCBuZXh0V29yZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2hvd1dvcmQobmV4dFdvcmQpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKCR3b3JkLnBhcmVudHMoJy5jZC1oZWFkbGluZScpLmhhc0NsYXNzKCdsb2FkaW5nLWJhcicpKXtcclxuICAgICAgICAgICAgICAgICR3b3JkLnBhcmVudHMoJy5jZC13b3Jkcy13cmFwcGVyJykucmVtb3ZlQ2xhc3MoJ2lzLWxvYWRpbmcnKTtcclxuICAgICAgICAgICAgICAgIHN3aXRjaFdvcmQoJHdvcmQsIG5leHRXb3JkKTtcclxuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXsgaGlkZVdvcmQobmV4dFdvcmQpIH0sIGJhckFuaW1hdGlvbkRlbGF5KTtcclxuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXsgJHdvcmQucGFyZW50cygnLmNkLXdvcmRzLXdyYXBwZXInKS5hZGRDbGFzcygnaXMtbG9hZGluZycpIH0sIGJhcldhaXRpbmcpO1xyXG5cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHN3aXRjaFdvcmQoJHdvcmQsIG5leHRXb3JkKTtcclxuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXsgaGlkZVdvcmQobmV4dFdvcmQpIH0sIGFuaW1hdGlvbkRlbGF5KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gc2hvd1dvcmQoJHdvcmQsICRkdXJhdGlvbikge1xyXG4gICAgICAgICAgICBpZigkd29yZC5wYXJlbnRzKCcuY2QtaGVhZGxpbmUnKS5oYXNDbGFzcygndHlwZScpKSB7XHJcbiAgICAgICAgICAgICAgICBzaG93TGV0dGVyKCR3b3JkLmZpbmQoJ2knKS5lcSgwKSwgJHdvcmQsIGZhbHNlLCAkZHVyYXRpb24pO1xyXG4gICAgICAgICAgICAgICAgJHdvcmQuYWRkQ2xhc3MoJ2lzLXZpc2libGUnKS5yZW1vdmVDbGFzcygnaXMtaGlkZGVuJyk7XHJcblxyXG4gICAgICAgICAgICB9ICBlbHNlIGlmKCR3b3JkLnBhcmVudHMoJy5jZC1oZWFkbGluZScpLmhhc0NsYXNzKCdjbGlwJykpIHtcclxuICAgICAgICAgICAgICAgICR3b3JkLnBhcmVudHMoJy5jZC13b3Jkcy13cmFwcGVyJykuYW5pbWF0ZSh7ICd3aWR0aCcgOiAkd29yZC53aWR0aCgpICsgMTAgfSwgcmV2ZWFsRHVyYXRpb24sIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpeyBoaWRlV29yZCgkd29yZCkgfSwgcmV2ZWFsQW5pbWF0aW9uRGVsYXkpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGhpZGVMZXR0ZXIoJGxldHRlciwgJHdvcmQsICRib29sLCAkZHVyYXRpb24pIHtcclxuICAgICAgICAgICAgJGxldHRlci5yZW1vdmVDbGFzcygnaW4nKS5hZGRDbGFzcygnb3V0Jyk7XHJcblxyXG4gICAgICAgICAgICBpZighJGxldHRlci5pcygnOmxhc3QtY2hpbGQnKSkge1xyXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpeyBoaWRlTGV0dGVyKCRsZXR0ZXIubmV4dCgpLCAkd29yZCwgJGJvb2wsICRkdXJhdGlvbik7IH0sICRkdXJhdGlvbik7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZigkYm9vbCkge1xyXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpeyBoaWRlV29yZCh0YWtlTmV4dCgkd29yZCkpIH0sIGFuaW1hdGlvbkRlbGF5KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYoJGxldHRlci5pcygnOmxhc3QtY2hpbGQnKSAmJiAkKCdodG1sJykuaGFzQ2xhc3MoJ25vLWNzc3RyYW5zaXRpb25zJykpIHtcclxuICAgICAgICAgICAgICAgIHZhciBuZXh0V29yZCA9IHRha2VOZXh0KCR3b3JkKTtcclxuICAgICAgICAgICAgICAgIHN3aXRjaFdvcmQoJHdvcmQsIG5leHRXb3JkKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gc2hvd0xldHRlcigkbGV0dGVyLCAkd29yZCwgJGJvb2wsICRkdXJhdGlvbikge1xyXG4gICAgICAgICAgICAkbGV0dGVyLmFkZENsYXNzKCdpbicpLnJlbW92ZUNsYXNzKCdvdXQnKTtcclxuXHJcbiAgICAgICAgICAgIGlmKCEkbGV0dGVyLmlzKCc6bGFzdC1jaGlsZCcpKSB7XHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7IHNob3dMZXR0ZXIoJGxldHRlci5uZXh0KCksICR3b3JkLCAkYm9vbCwgJGR1cmF0aW9uKTsgfSwgJGR1cmF0aW9uKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGlmKCR3b3JkLnBhcmVudHMoJy5jZC1oZWFkbGluZScpLmhhc0NsYXNzKCd0eXBlJykpIHsgc2V0VGltZW91dChmdW5jdGlvbigpeyAkd29yZC5wYXJlbnRzKCcuY2Qtd29yZHMtd3JhcHBlcicpLmFkZENsYXNzKCd3YWl0aW5nJyk7IH0sIDIwMCk7fVxyXG4gICAgICAgICAgICAgICAgaWYoISRib29sKSB7IHNldFRpbWVvdXQoZnVuY3Rpb24oKXsgaGlkZVdvcmQoJHdvcmQpIH0sIGFuaW1hdGlvbkRlbGF5KSB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIHRha2VOZXh0KCR3b3JkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAoISR3b3JkLmlzKCc6bGFzdC1jaGlsZCcpKSA/ICR3b3JkLm5leHQoKSA6ICR3b3JkLnBhcmVudCgpLmNoaWxkcmVuKCkuZXEoMCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmdW5jdGlvbiB0YWtlUHJldigkd29yZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gKCEkd29yZC5pcygnOmZpcnN0LWNoaWxkJykpID8gJHdvcmQucHJldigpIDogJHdvcmQucGFyZW50KCkuY2hpbGRyZW4oKS5sYXN0KCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmdW5jdGlvbiBzd2l0Y2hXb3JkKCRvbGRXb3JkLCAkbmV3V29yZCkge1xyXG4gICAgICAgICAgICAkb2xkV29yZC5yZW1vdmVDbGFzcygnaXMtdmlzaWJsZScpLmFkZENsYXNzKCdpcy1oaWRkZW4nKTtcclxuICAgICAgICAgICAgJG5ld1dvcmQucmVtb3ZlQ2xhc3MoJ2lzLWhpZGRlbicpLmFkZENsYXNzKCdpcy12aXNpYmxlJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBtZXNzYWdlRmxhc2hcclxuICAgICAgICBsZXQgbWVzc2FnZWZsYXNoID0gJChcIi5hbGVydFwiKTtcclxuICAgICAgICBtZXNzYWdlZmxhc2guZGVsYXkoMzAwMCkuc2xpZGVVcCgzMDApO1xyXG5cclxuICAgICAgICAvLyBuYXZiYXIgbWVudVxyXG4gICAgICAgICQoJy5zaWRlLW5hdicpLnNpZGVuYXYoXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIG1lbnVXaWR0aDogMjQwLCAvLyBEZWZhdWx0IGlzIDI0MFxyXG4gICAgICAgICAgICAgICAgY2xvc2VPbkNsaWNrOiB0cnVlLCAvLyBDbG9zZXMgc2lkZS1uYXYgb24gPGE+IGNsaWNrcywgdXNlZnVsIGZvciBBbmd1bGFyL01ldGVvclxyXG4gICAgICAgICAgICAgICAgcHJldmVudFNjcm9sbGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICQoJy5zaWRlbmF2LW92ZXJsYXknKS5zaG93KClcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICAvLyBzY3JvbGwgdG8gdG9wXHJcbiAgICAgICAgJCgnLmJ0bi1zY3JvbGx0b3AnKS5jbGljayhmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgJChcImh0bWwsIGJvZHlcIikuYW5pbWF0ZSh7IHNjcm9sbFRvcDogMCB9LCBcInNsb3dcIik7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9KTtcclxuICAgICAgICAkKCcubW9kYWwnKS5tb2RhbCgpO1xyXG4gICAgICAgIC8vc2Nyb2xsIG5hdmlnYXRpb25cclxuICAgICAgICAkKCcuc2Nyb2xsc3B5Jykuc2Nyb2xsU3B5KCk7XHJcblxyXG4gICAgICAgIC8vIHJlbmRyZSBsZXMgaW1hZ2VzIHJlc3BvbnNpdmVcclxuICAgICAgICAkKCdpbWcnKS5hZGRDbGFzcygncmVzcG9uc2l2ZS1pbWcnKTtcclxuXHJcbiAgICAgICAgLy9iYWNrZ3JvdW5kIG5hdmlnYXRpb24gb24gc2Nyb2xsXHJcbiAgICAgICAgJChkb2N1bWVudCkuc2Nyb2xsKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICB2YXIgaW5kZXhCYW5uZXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnaW5kZXgtYmFubmVyJykub2Zmc2V0SGVpZ2h0O1xyXG4gICAgICAgICAgICB2YXIgc2Nyb2xsVG9wID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHNjcm9sbFRvcCA+PSBpbmRleEJhbm5lcikge1xyXG4gICAgICAgICAgICAgICAgJCgnI25hdl9ob21lJykuY3NzKCdiYWNrZ3JvdW5kJywgJyMyODI4Mjg5NScpO1xyXG4gICAgICAgICAgICB9ZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAkKCcjbmF2X2hvbWUnKS5jc3MoJ2JhY2tncm91bmQnLCAndHJhbnNwYXJlbnQnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBmYWlyZSBhcHBhcmFpdHJlIGxlIGJvdXRvbiBzY3JvbGx0b3BcclxuICAgICAgICAkKCcjc2Nyb2xsdG9wJykuaGlkZSgpO1xyXG4gICAgICAgICQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAkKHdpbmRvdykuc2Nyb2xsKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIGlmICgkKHRoaXMpLnNjcm9sbFRvcCgpID4gNDAwICkge1xyXG4gICAgICAgICAgICAgICAgICAgICQoJyNzY3JvbGx0b3AnKS5mYWRlSW4oNTAwKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJCgnI3Njcm9sbHRvcCcpLmZhZGVPdXQoNTAwKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH0pOyAvLyBlbmQgb2YgZG9jdW1lbnQgcmVhZHlcclxufSkoalF1ZXJ5KTsgLy8gZW5kIG9mIGpRdWVyeSBuYW1lIHNwYWNlXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vYXNzZXRzL2pzL2FwcC5qcyJdLCJzb3VyY2VSb290IjoiIn0=
>>>>>>> dev
