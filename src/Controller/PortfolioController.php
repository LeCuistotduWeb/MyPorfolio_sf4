<?php

namespace App\Controller;

use App\Entity\Work;
use App\Repository\CategoryRepository;
use App\Repository\WorkRepository;
use App\Service\mailer\Mailer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PortfolioController extends Controller
{
    /**
     * @Route("/", name="portfolio_home")
     */
    public function index(WorkRepository $workRepository, Request $request,Session $session, Mailer $mailer, CategoryRepository $categoryRepository)
    {
        $formContact = $this->createForm('App\Form\ContactType', null ,[]);
        if ($request->isMethod('POST')) {
            // Refill the fields in case the form is not valid.
            $formContact->handleRequest($request);

            if($formContact->isSubmitted() && $formContact->isValid()){
                // Send mail
                $mailer->sendEmail($formContact->getData());
                $this->addFlash('success', 'Votre message m\'à bien été envoyé. Je vous répondrai dans les plus brefs délais ');
                unset($formContact);
                $formContact = $this->createForm('App\Form\ContactType', null ,[]);
            }
        }

        return $this->render('portfolio/index.html.twig', [
            'works' => $workRepository->findByWorkPublished(),
            'form' => $formContact->createView(),
            'categories' => $categoryRepository->findAll(),
        ]);
    }

    /**
     * @Route("/project/{id}", name="show_project")
     */
    public function show(Work $work)
    {
        return $this->render('portfolio/simple.html.twig', ['work' => $work]);
    }

    /**
     * @Route("/mentions_legales", name="mention_legales")
     */
    public function mentions()
    {
        return $this->render('portfolio/mentionslegales.html.twig');
    }

    /**
     * @Route("/politique_de_confidentialite", name="politique_de_confidentialite")
     */
    public function politiqueDeConfidentialite()
    {
        return $this->render('portfolio/politiqueconfidentialite.html.twig');
    }
}
