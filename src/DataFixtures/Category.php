<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Category extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $tagArray = ['Développement Web','Infographie','Webdesign'];

        for ($i = 0; $i < count($tagArray); $i++) {
            $Category = new \App\Entity\Category();
            $Category->setName($tagArray[$i]);
            $manager->persist($Category);
        }

        $manager->flush();
    }
}
