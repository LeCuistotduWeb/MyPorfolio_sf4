<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Tag extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $tagArray = ['html','css','javascript','php','photoshop','inDesign'];

        for ($i = 0; $i < count($tagArray); $i++) {
            $tag = new \App\Entity\Tag();
            $tag->setName($tagArray[$i]);
            $manager->persist($tag);
        }

        $manager->flush();
    }
}
