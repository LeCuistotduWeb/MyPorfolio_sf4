<?php

namespace App\Form;

use App\Entity\Work;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WorkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('excerpt', TextareaType::class)
            ->add('content', TextareaType::class, ['attr' => ['class' => 'ckeditor']])
            ->add('image', ImageType::class)
            ->add('categories', EntityType::class, array(
                'class'        => 'App\Entity\Category',
                'choice_label' => 'name',
                'multiple'     => true,
            ))
            ->add('tags', EntityType::class, array(
                'class'        => 'App\Entity\Tag',
                'choice_label' => 'name',
                'multiple'     => true,
            ))
            ->add('published', CheckboxType::class, [
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Work::class,
        ]);
    }
}
