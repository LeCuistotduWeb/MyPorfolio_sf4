<?php

namespace App\Service\mailer;

class Mailer extends \Twig_Extension
{
    private $mailer;
    private $templating;

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    public function sendEmail($data){
        $myappContactMail = 'gaetan.boyron@gmail.com';

        $message = (new \Swift_Message('Hello Email'))
            ->setFrom([$myappContactMail => "Message by ".$data["name"]])
            ->setTo([$myappContactMail => $myappContactMail])
            ->setBody($this->templating->render('emails/contact.html.twig', [
                'data' => $data
            ]))
            ->setcharset('utf-8')
            ->setContentType("text/html")
        ;
        $this->mailer->send($message);
    }
}